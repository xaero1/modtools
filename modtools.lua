script_name("ModTools")
script_version("2.0")
require 'lib.moonloader'
local memory = require('memory')
local Matrix3X3 = require "matrix3x3"
local Vector3D = require "vector3d"
local imgui = require 'imgui'
local sampev = require 'samp.events'
local cjson = require"cjson"
local effil = require"effil"
local ffi = require"ffi"
local getBonePosition = ffi.cast("int (__thiscall*)(void*, float*, int, bool)", 0x5E4280)
local encoding = require('encoding') 
local u8 = encoding.UTF8
encoding.default = 'ISO-8859-1'
local rkeys = require 'rkeys'
local fa = require 'fAwesome5'
local fa_font = nil
local fa_font2 = nil
local fa_glyph_ranges = imgui.ImGlyphRanges({ fa.min_range, fa.max_range })
imgui.HotKey = require('imgui_addons').HotKey

fileS = "moonloader/config/modtools.sus"
notifytable = {}
notifydb = io.open(fileS, "a+")
tempList = {}
for todo in notifydb.lines(notifydb) do
    local nombre, razon = todo:match("^(%S+)%s*(.-)$")
    table.insert(notifytable, {
		name = nombre:lower(),
		reason = razon,
    })
end
io.close(notifydb)
fileL = "moonloader/ListaMODTOOLS.lst"
local commaf = imgui.ImBuffer(1024)
if not doesFileExist(fileL) then
cmdfile = io.open(fileL, "w")
io.output(cmdfile)
io.write("")
io.close(cmdfile)
end
cmdtable = {}
cmdfile = io.open(fileL, "a+")
for todo in cmdfile.lines(cmdfile) do
    local com = todo:match("^(%a+)")
	if com and com ~= "a" then
    table.insert(cmdtable, com)
	end
end
io.close(cmdfile)

cmdfile = io.open(fileL, "r")
commaf.v = cmdfile:read('*all')
io.close(cmdfile)


local inicfg = require 'inicfg'
local directIni = "modtools.ini"
local ini = inicfg.load(inicfg.load({
    settings = {
		fastr = true,
		susreg = true,
		bloqchat = false,
		wh = false,
		whesq = false,
		key = "[18,50]",
		whkey = "[114]"
    },
}, directIni))
inicfg.save(ini, directIni)
if not ini.settings.whkey or ini.settings.whkey == "null" then
	ini.settings.whkey =  "[114]"
	inicfg.save(ini, directIni)
end
local ActiveMenu = {
    v = decodeJson(ini.settings.key)
}
local ActiveWall = {
	v = decodeJson(ini.settings.whkey) or "[114]"
}
local CCamera = 0xB6F028
local distorg = 0
keyToggle = VK_MBUTTON
keyApply = VK_LBUTTON
local tLastKeys = {}
local whLastKeys = {}
local bindID = 0
local bindwhID = 1
local onetime = true
local aggcmdchat = false
local guardado = false
local mainwindow = imgui.ImBool(false)
local suspe = imgui.ImBool(false)
local nivelc = imgui.ImBuffer(256)
local infracionesc = imgui.ImBuffer(256)
local susnam = imgui.ImBuffer(256)
local tiemporev
local susraz = imgui.ImBuffer(256)
local tiempoc = imgui.ImBuffer(256)
local resultado = ''
local text2 = ''
local isvpn = ''
local ip1 = ''
local ipc = imgui.ImBuffer(256)
local fastspawn = imgui.ImBool(ini.settings.fastr)
local toggle = imgui.ImBool(ini.settings.susreg)
local bloqchat = imgui.ImBool(ini.settings.bloqchat)
local wallhack = imgui.ImBool(ini.settings.wh)
local whesque = imgui.ImBool(ini.settings.whesq)
local tag = "{82807f}[ModTools] {ffffff}"
local WHKJail = "https://discord.com/api/webhooks/1226668576644010074/g8jOpzI1L0oJVPQDbXO8Mtsr6R6zpM2qJyw2yId2SiZe1EQruO30eVhUc2PCK8kBkSdf"
local WHKAdv = "https://discord.com/api/webhooks/1226668646126977055/_2wbh4fiOs8xVSWmxRgCOIlBm0VhKFw-MJyeYmd2lRfkhpUwmuDEsQFN1HaIAdHpFt_o"
local WHKBan = "https://discord.com/api/webhooks/1226668709217570848/AeOMZT37g0I9KM5F7vRKhc682sNY1M3kpIOIwxEi7huESEmo2v909QbkT8gKCMbmx8VD"
local WHKTban = "https://discord.com/api/webhooks/1226668764318142515/YcYp2CC2xiYQJfzUc4laOqlQewxX0Zozndo58la1WmyLQJcP3hRKyTjFW_xmVBrjI4pe"
local WHKUnban = "https://discord.com/api/webhooks/1226668827128103003/29Dwzb-nSN3OyXJfPhHc9SS1y91n7qGZKmj051jWvhUZhCWjXMDYviTXaGZeYetuBv0v"
local WHKKick = "https://discord.com/api/webhooks/1226668884783009992/OpmMld91b40IP3YcKsOhk_oqN_XWssG_05z4UyH0qM1sa2mUtktiqI0CMN4dJlhhMFok"

function SendWebhook(URL, DATA, callback_ok, callback_error)
    local function asyncHttpRequest(method, url, args, resolve, reject)
        local request_thread = effil.thread(function (method, url, args)
           local requests = require 'requests'
           local result, response = pcall(requests.request, method, url, args)
           if result then
              response.json, response.xml = nil, nil
              return true, response
           else
              return false, response
           end
        end)(method, url, args)
        if not resolve then resolve = function() end end
        if not reject then reject = function() end end
        lua_thread.create(function()
            local runner = request_thread
            while true do
                local status, err = runner:status()
                if not err then
                    if status == 'completed' then
                        local result, response = runner:get()
                        if result then
                           resolve(response)
                        else
                           reject(response)
                        end
                        return
                    elseif status == 'canceled' then
                        return reject(status)
                    end
                else
                    return reject(err)
                end
                wait(0)
            end
        end)
    end
    asyncHttpRequest('POST', URL, {headers = {['content-type'] = 'application/json'}, data = u8(DATA)}, callback_ok, callback_error)
end

local memory = require("memory")


local set = {
	player_tags_dist = 350.0, -- Дальность wallhack
	font_shadow = 1 -- Отрисовка тени возле шрифтов. 0/1 - вкл/выкл соответственно
}


local pD3DFont_sampStuff = renderCreateFont("Tahoma", 10, 0x4) -- FCR_BORDER
local pD3DFontFixedSmall = renderCreateFont("Small Fonts", 8, 0x4) -- FCR_BORDER
local g_dwSAMP_Addr = -1
local g_SAMP = -1
local g_Players = -1
local g_playerTagInfo = {}
for i = 0, 1004 do
	g_playerTagInfo[i] = {tagPosition = {fX = 0.0, fY = 0.0, fZ = 0.0}, tagOffsetY = 0.0, isStairStacked = false, stairStackedOffset = 0.0, isPastMaxDistance = false}
end

-- Оффсеты
local SAMP_INFO_OFFSET = {
	[1] = 0x21A0F8,
	[2] = 0x21A100,
	[3] = 0x2ACA24
}
local SAMP_SCOREBOARD_INFO =  {
	[1] = 0x21A0B4,
	[2] = 0x21A0BC,
	[3] = 0x2AC9DC
}
local SAMP_PPOOLS_OFFSET = {
	[1] = 0x3CD,
	[2] = 0x3C5,
	[3] = 0x3DE
}
local SAMP_PPOOL_PLAYER_OFFSET = {
	[1] = 0x18,
	[2] = 0x8,
	[3] = 0x8
}

-- Начало модуля с функциями
local function isVehicleClassSmall(vehicleHandle)
	if vehicleHandle ~= nil then
		local vehicleModelId = getCarModel(vehicleHandle)
		for i, v in ipairs({441, 457, 464, 465, 485, 501, 530, 564, 571, 572, 574, 583, 594}) do -- VEHICLE_CLASS_MINI
			if v == vehicleModelId then
				return true
			end
		end
	end
	return false
end

local function join_argb(a, r, g, b)
	if a ~= nil and r ~= nil and g ~= nil and b ~= nil then
		local argb = b  -- b
		argb = bit.bor(argb, bit.lshift(g, 8))  -- g
		argb = bit.bor(argb, bit.lshift(r, 16)) -- r
		argb = bit.bor(argb, bit.lshift(a, 24)) -- a
		return argb
	end
end

local function explode_argb(argb)
	if argb ~= nil then
		local a = bit.band(bit.rshift(argb, 24), 0xFF)
		local r = bit.band(bit.rshift(argb, 16), 0xFF)
		local g = bit.band(bit.rshift(argb, 8), 0xFF)
		local b = bit.band(argb, 0xFF)
		return a, r, g, b
	end
end

local function CalcScreenCoors(fX, fY, fZ)
	if fX ~= nil and fY ~= nil and fZ ~= nil then
		-- C++-ifyed function 0x71DA00, formerly called by CHudSA::CalcScreenCoors
		-- Get the static view matrix
		local dwM = 0xB6FA2C

		local m_11 = memory.getfloat(dwM + 0 * 4, true)
		local m_12 = memory.getfloat(dwM + 1 * 4, true)
		local m_13 = memory.getfloat(dwM + 2 * 4, true)

		local m_21 = memory.getfloat(dwM + 4 * 4, true)
		local m_22 = memory.getfloat(dwM + 5 * 4, true)
		local m_23 = memory.getfloat(dwM + 6 * 4, true)

		local m_31 = memory.getfloat(dwM + 8 * 4, true)
		local m_32 = memory.getfloat(dwM + 9 * 4, true)
		local m_33 = memory.getfloat(dwM + 10 * 4, true)

		local m_41 = memory.getfloat(dwM + 12 * 4, true)
		local m_42 = memory.getfloat(dwM + 13 * 4, true)
		local m_43 = memory.getfloat(dwM + 14 * 4, true)

		-- Get the static virtual screen (x,y)-sizes
		local dwLenX = memory.getuint32(0xC17044, true)
		local dwLenY = memory.getuint32(0xC17048, true)

		-- Do a transformation
		local frX = fZ * m_31 + fY * m_21 + fX * m_11 + m_41
		local frY = fZ * m_32 + fY * m_22 + fX * m_12 + m_42
		local frZ = fZ * m_33 + fY * m_23 + fX * m_13 + m_43

		-- Get the correct screen coordinates
		local fRecip = 1.0 / frZ
		frX = frX * (fRecip * dwLenX)
		frY = frY * (fRecip * dwLenY)
		return frX, frY, frZ
	end
end

local function PrintShadow(font, text, x, y, color)
	if font ~= nil and text ~= nil and x ~= nil and y ~= nil and color ~= nil then
		if set.font_shadow == 1 then
			local tempColor = bit.band(bit.rshift(color, 16), 0xffff) -- HIWORD
			tempColor = bit.band(bit.rshift(tempColor, 8), 0xffff) -- HIBYTE
			local shadow = join_argb(tempColor, 0, 0, 0)
			renderFontDrawText(font, text, x + 1, y + 1, shadow)
		end
		renderFontDrawText(font, text, x, y, color)
	end
end

local function getCharCoordinatesFixed(ped)
	if ped ~= nil then
		local dwPED = getCharPointer(ped) + 0x0
		local dwAddress = memory.getuint32(dwPED + 0x14, true)
		local fX = memory.getfloat(dwAddress + 0x30, true)
		local fY = memory.getfloat(dwAddress + 0x34, true)
		local fZ = memory.getfloat(dwAddress + 0x38, true)
		return fX, fY, fZ
	end
end

local function getSampDll()
	local g_dwSAMP_Addr = getModuleHandle("samp.dll")
	if g_dwSAMP_Addr ~= nil then
		return g_dwSAMP_Addr
	end
	return -1
end

local function getSampVersion()
	local g_dwSAMP_Addr = getModuleHandle("samp.dll")
	if g_dwSAMP_Addr ~= nil then
		local version = memory.getuint8(g_dwSAMP_Addr + 0x1036, true)
		if version ~= nil then
			return (version == 0xD8 and 1) or (version == 0xA8 and 2) or (version == 0x78 and 3)
		end
	end
	return -1
end

local function getSampInfoStruct()
	local g_dwSAMP_Addr = getModuleHandle("samp.dll")
	if g_dwSAMP_Addr ~= nil then
		local sampVer = getSampVersion()
		if sampVer ~= -1 then
			local g_SAMP = memory.getuint32(g_dwSAMP_Addr + SAMP_INFO_OFFSET[sampVer], true)
			if g_SAMP ~= nil then
				return g_SAMP
			end
		end
	end
	return -1
end

local function isSampScoreboardOpened()
	local g_dwSAMP_Addr = getModuleHandle("samp.dll")
	if g_dwSAMP_Addr ~= nil then
		local sampVer = getSampVersion()
		if sampVer ~= -1 then
			local scoreboard_struct = memory.getuint32(g_dwSAMP_Addr + SAMP_SCOREBOARD_INFO[sampVer], true)
			if scoreboard_struct ~= nil then
				local represent = memory.getint8(scoreboard_struct, true)
				if represent ~= nil then
					if represent == 1 then
						return true
					elseif represent == 0 then
						return false
					end
				end
			end
		end
	end
	return -1
end

local function getSampPlayerPoolStruct()
	local g_SAMP = getSampInfoStruct()
	if g_SAMP ~= nil then
		local sampVer = getSampVersion()
		if sampVer ~= -1 then
			local ppools = memory.getuint32(g_SAMP + SAMP_PPOOLS_OFFSET[sampVer], true)
			if ppools ~= nil then
				local g_Players = memory.getuint32(ppools + SAMP_PPOOL_PLAYER_OFFSET[sampVer], true)
				if g_Players ~= nil then
					return g_Players
				end
			end
		end
	end
	return -1
end

function main()
    while not isSampAvailable() do wait(0) end

	sampAddChatMessage(tag .."Autor: {82807f}Xaero{FFFFFF}. Para abrir el menu usa: {82807f}/mt{FFFFFF} o presiona: {82807f}" .. table.concat(rkeys.getKeysName(ActiveMenu.v), " + "), -1)

	sampRegisterChatCommand("jail", function (arg)
	local idj, tiempo, multa, razon = arg:match("(%d+) (%d+) (%d+) (.*)")
	local MyId = select(2, sampGetPlayerIdByCharHandle(PLAYER_PED))
    local MyName = sampGetPlayerNickname(MyId)
	if idj and tiempo and multa and razon then
		sampSendChat("/jail "..idj.." "..tiempo.." "..multa.." "..razon)
		print("Comando enviado: /jail "..idj.." "..tiempo.." "..multa.." "..razon)
		local jailnam = sampGetPlayerNickname(idj)
		local multaM = tiempo * 100
			if tiempo == "0" then 
				SendWebhook(WHKJail, ([[{
					"content": null,
					"embeds": [
					{
						"title": "Sancion / Carcel Retirada",
						"description": "**Moderador:**  `%s`\n**Usuario:** `%s`\n**Razon:** `%s`\n**Fecha:** `%s`",
						"color": 16745728
					}
					],
					"avatar_url": "https://i.ibb.co/0Y679tL/gxrplogo.png",
					"attachments": []
				}]]):format(MyName, jailnam, razon, os.date("%I:%M%p %d/%m/%y")))
			else
				SendWebhook(WHKJail, ([[{
					"content": null,
					"embeds": [
					{
						"title": "Sancion",
						"description": "**Moderador:**  `%s`\n**Usuario:** `%s`\n**Razon:** `%s`\n**Tiempo:** `%s minutos`\n**Multa:** `%s$`\n**Fecha:** `%s`",
						"color": 16734039
					}
					],
					"avatar_url": "https://i.ibb.co/0Y679tL/gxrplogo.png",
					"attachments": []
				}]]):format(MyName, jailnam, razon, tiempo, multaM, os.date("%I:%M%p %d/%m/%y")))
		end
	else 
		sampAddChatMessage("Uso: /jail [ID] [Tiempo] [Multa. 1: Si | 0: No] [Razon]", -1)
	end
end)
    sampRegisterChatCommand("jailex", function (arg)
	local nmj, OOC, tiempo, razon = arg:match("^(%S+)%s+(%d+)%s+(%d+)%s+(.*)")
	local MyId = select(2, sampGetPlayerIdByCharHandle(PLAYER_PED))
    local MyName = sampGetPlayerNickname(MyId)
	local OOCc = ""
	if OOC == 1 then
		OOCc = "OOC"
	else
		OOCc = "IC"
	end
	if nmj and OOC and tiempo and razon then
		sampSendChat("/jailex "..nmj.." "..OOC.." "..tiempo.." "..razon)
		print("Comando enviado: /jailex "..nmj.." "..OOC.." "..tiempo.." "..razon)
			SendWebhook(WHKJail, ([[{
				"content": null,
				"embeds": [
				{
					"title": "Sancion OFF",
					"description": "**Moderador:**  `%s`\n**Usuario:** `%s`\n**Razon:** `%s`\n**Tiempo:** `%s minutos`\n**Tipo:** `%s`\n**Fecha:** `%s`",
					"color": 16711758
				}
				],
				"avatar_url": "https://i.ibb.co/0Y679tL/gxrplogo.png",
				"attachments": []
			}]]):format(MyName, nmj, razon, tiempo, OOCc, os.date("%I:%M%p %d/%m/%y")))
	else 
		sampAddChatMessage("Uso: /jailex [Nombre_Apellido] [1: OOC] [Tiempo] [Razon]", -1)
	end
end)
    sampRegisterChatCommand("advertir", function (arg)
	local idj, razon = arg:match("(%d+) (.*)")
	local MyId = select(2, sampGetPlayerIdByCharHandle(PLAYER_PED))
    local MyName = sampGetPlayerNickname(MyId)
	if idj and razon then
		sampSendChat("/advertir "..idj.." "..razon)
		print("Comando enviado: /advertir "..idj.." "..razon)
		local advnm = sampGetPlayerNickname(idj)
			SendWebhook(WHKAdv, ([[{
				"content": null,
				"embeds": [
				{
					"title": "Advertencia",
					"description": "**Moderador:**  `%s`\n**Usuario:** `%s`\n**Razon:** `%s`\n**Fecha:** `%s`",
					"color": 16763904
				}
				],
				"avatar_url": "https://i.ibb.co/0Y679tL/gxrplogo.png",
				"attachments": []
			}]]):format(MyName, advnm, razon, os.date("%I:%M%p %d/%m/%y")))
	else 
		sampAddChatMessage("Uso: /advertir [ID] [Razon]", -1)
	end
end)
    sampRegisterChatCommand("quitaradv", function (arg)
	local idj, cant, razon = arg:match("(%d+) (%d+) (.*)")
	local MyId = select(2, sampGetPlayerIdByCharHandle(PLAYER_PED))
    local MyName = sampGetPlayerNickname(MyId)
	if idj and cant and razon then
		sampSendChat("/quitaradv "..idj.." "..cant.." "..razon)
		print("Comando enviado: /quitaradv "..idj.." "..cant.." "..razon)
		local advnm = sampGetPlayerNickname(idj)
			SendWebhook(WHKAdv, ([[{
				"content": null,
				"embeds": [
				{
					"title": "Advertencia Retirada",
					"description": "**Moderador:**  `%s`\n**Usuario:** `%s`\n**Cantidad:** `%s`\n**Razon:** `%s`\n**Fecha:** `%s`",
					"color": 16763904
				}
				],
				"avatar_url": "https://i.ibb.co/0Y679tL/gxrplogo.png",
				"attachments": []
			}]]):format(MyName, advnm, cant, razon, os.date("%I:%M%p %d/%m/%y")))
	else 
		sampAddChatMessage("Uso: /quitaradv [ID] [Cantidad] [Razon]", -1)
	end
end)
    sampRegisterChatCommand("kick", function (arg)
	local idj, razon = arg:match("(%d+) (.*)")
	local MyId = select(2, sampGetPlayerIdByCharHandle(PLAYER_PED))
    local MyName = sampGetPlayerNickname(MyId)
	if idj and razon then
		sampSendChat("/kick "..idj.." "..razon)

		print("Comando enviado: /kick "..idj.." "..razon)
		local kcknm = sampGetPlayerNickname(idj)
			SendWebhook(WHKKick, ([[{
				"content": null,
				"embeds": [
				{
					"title": "Kick",
					"description": "**Moderador:**  `%s`\n**Usuario:** `%s`\n**Razon:** `%s`\n**Fecha:** `%s`",
					"color": 1310848
				}
				],
				"avatar_url": "https://i.ibb.co/0Y679tL/gxrplogo.png",
				"attachments": []
			}]]):format(MyName, kcknm, razon, os.date("%I:%M%p %d/%m/%y")))
	else 
		sampAddChatMessage("Uso: /kick [ID] [Razon]", -1)
	end
end)
    sampRegisterChatCommand("tban", function (arg)
	local idj, tiempo, razon = arg:match("(%d+) (%d+) (.*)")
	local MyId = select(2, sampGetPlayerIdByCharHandle(PLAYER_PED))
    local MyName = sampGetPlayerNickname(MyId)
	if idj and tiempo and razon then
		sampSendChat("/tban "..idj.." "..tiempo.." "..razon)

		print("Comando enviado: /tban "..idj.." "..tiempo.." "..razon)
			local tbnm = sampGetPlayerNickname(idj)
			SendWebhook(WHKTban, ([[{
				"content": null,
				"embeds": [
				{
					"title": "Baneo Temporal",
					"description": "**Moderador:**  `%s`\n**Usuario:** `%s`\n**Tiempo:** `%s horas`\n**Razon:** `%s`\n**Fecha:** `%s`",
					"color": 11796480
				}
				],
				"avatar_url": "https://i.ibb.co/0Y679tL/gxrplogo.png",
				"attachments": []
			}]]):format(MyName, tbnm, tiempo, razon, os.date("%I:%M%p %d/%m/%y")))
	else 
		sampAddChatMessage("Uso: /tban [ID] [Tiempo] [Razon]", -1)
	end
end)
	sampRegisterChatCommand("tbanex", function (arg)
	local nmb, tiempo, razon = arg:match("^(%S+)%s+(%d+)%s+(.*)")
	local MyId = select(2, sampGetPlayerIdByCharHandle(PLAYER_PED))
    local MyName = sampGetPlayerNickname(MyId)
	if nmb and tiempo and razon then
		sampSendChat("/tbanex "..nmb.." "..tiempo.." "..razon)

		print("Comando enviado: /tbanex "..nmb.." "..tiempo.." "..razon)
			SendWebhook(WHKTban, ([[{
				"content": null,
				"embeds": [
				{
					"title": "Baneo Temporal OFF",
					"description": "**Moderador:**  `%s`\n**Usuario:** `%s`\n**Tiempo:** `%s horas`\n**Razon:** `%s`\n**Fecha:** `%s`",
					"color": 11796480
				}
				],
				"avatar_url": "https://i.ibb.co/0Y679tL/gxrplogo.png",
				"attachments": []
			}]]):format(MyName, nmb, tiempo, razon, os.date("%I:%M%p %d/%m/%y")))
	else 
		sampAddChatMessage("Uso: /tbanex [Nombre_Apellido] [Tiempo] [Razon]", -1)
	end
end)
    sampRegisterChatCommand("ban", function (arg)
	local idj, razon = arg:match("(%d+) (.*)")
	local MyId = select(2, sampGetPlayerIdByCharHandle(PLAYER_PED))
    local MyName = sampGetPlayerNickname(MyId)
	if idj and razon then
		sampSendChat("/ban "..idj.." "..razon)

		print("Comando enviado: /ban "..idj.." "..razon)
			local bnm = sampGetPlayerNickname(idj)
			SendWebhook(WHKBan, ([[{
				"content": null,
				"embeds": [
				{
					"title": "Baneo Permanente",
					"description": "**Moderador:**  `%s`\n**Usuario:** `%s`\n**Razon:** `%s`\n**Fecha:** `%s`",
					"color": 16711680
				}
				],
				"avatar_url": "https://i.ibb.co/0Y679tL/gxrplogo.png",
				"attachments": []
			}]]):format(MyName, bnm, razon, os.date("%I:%M%p %d/%m/%y")))
	else 
		sampAddChatMessage("Uso: /ban [ID] [Razon]", -1)
	end
end)
	sampRegisterChatCommand("banex", function (arg)
	local nmb, razon = arg:match("^(%S+)%s+(.*)")
	local MyId = select(2, sampGetPlayerIdByCharHandle(PLAYER_PED))
    local MyName = sampGetPlayerNickname(MyId)
	if nmb and razon then
		sampSendChat("/banex "..nmb.." "..razon)

		print("Comando enviado: /banex "..nmb.." "..razon)
			SendWebhook(WHKBan, ([[{
				"content": null,
				"embeds": [
				{
					"title": "Baneo Permanente OFF",
					"description": "**Moderador:**  `%s`\n**Usuario:** `%s`\n**Razon:** `%s`\n**Fecha:** `%s`",
					"color": 16711680
				}
				],
				"avatar_url": "https://i.ibb.co/0Y679tL/gxrplogo.png",
				"attachments": []
			}]]):format(MyName, nmb, razon, os.date("%I:%M%p %d/%m/%y")))
	else 
		sampAddChatMessage("Uso: /banex [Nombre_Apellido] [Razon]", -1)
	end
end)
	sampRegisterChatCommand("desban", function (arg)
	local nmb, razon = arg:match("^(%S+)%s+(.*)")
	local MyId = select(2, sampGetPlayerIdByCharHandle(PLAYER_PED))
    local MyName = sampGetPlayerNickname(MyId)
	if nmb and razon then
		sampSendChat("/desban "..nmb.." "..razon)

		print("Comando enviado: /desban "..nmb.." "..razon)
			SendWebhook(WHKUnban, ([[{
				"content": null,
				"embeds": [
				{
					"title": "Desbaneo",
					"description": "**Moderador:**  `%s`\n**Usuario:** `%s`\n**Razon:** `%s`\n**Fecha:** `%s`",
					"color": 3407616
				}
				],
				"avatar_url": "https://i.ibb.co/0Y679tL/gxrplogo.png",
				"attachments": []
			}]]):format(MyName, nmb, razon, os.date("%I:%M%p %d/%m/%y")))
	else 
		sampAddChatMessage("Uso: /desban [Nombre_Apellido] [Razon]", -1)
	end
end)
	sampRegisterChatCommand("mt", function ()
		mainwindow.v = not mainwindow.v
		if not imgui.ShowCursor then
			imgui.ShowCursor = true
		end
		imgui.Process = mainwindow.v
	end)
	bindID = rkeys.registerHotKey(ActiveMenu.v, true, function ()
		if not sampIsChatInputActive() or sampIsDialogActive() or isSampfuncsConsoleActive() then
			mainwindow.v = not mainwindow.v
		if not imgui.ShowCursor then
			imgui.ShowCursor = true
		end
			imgui.Process = mainwindow.v
		end
	end)
	bindwhID = rkeys.registerHotKey(ActiveWall.v, true, function ()
		ini.settings.wh = not ini.settings.wh
		wallhack = imgui.ImBool(ini.settings.wh)
		inicfg.save(ini, directIni)
	end)
	sampRegisterChatCommand("ssp", function ()
		suspe.v = not suspe.v
		if imgui.ShowCursor then
			imgui.ShowCursor = false
		end
		imgui.Process = suspe.v
	end)
	sampRegisterChatCommand("sosp", function (arg)
		local id, razon = arg:match("^(%d+)%s*(.-)$")
		if type(id) == "number" and sampIsPlayerConnected(id) and razon then
			local nombre = sampGetPlayerNickname(id)
			addnotify(nombre.." "..razon)
		else
			sampAddChatMessage(tag.." Uso: /sosp  [ ID ]  [ Razon ] .")
		end
	end)
	font = renderCreateFont("Tahoma", 10, FCR_BOLD + FCR_BORDER)
	while true do
	local tiempito 
	if not tiempito or tiempito <= os.time then
    tiempito = os.time() + 300
	end
	
	local ip, port = sampGetCurrentServerAddress()
	if  ip ~= '54.39.139.249' and onetime then
		sampAddChatMessage(tag.."Servidor 'Galaxy-RP' no detectado, apagando. En caso de ser un error informar a @xaeroxq en discord.", -1)
		onetime = false
		script:unload()
	end 
	if tiemporev and tiemporev <= os.time() then
		addcmd(commaf.v)
		aggcmdchat = false
		lua_thread.create(function() wait(1000) tiemporev = nil end)
	end
	if ini.settings.fastr and not sampIsChatInputActive() or sampIsDialogActive() or isSampfuncsConsoleActive() then
		if isKeyDown(keyToggle) then
			if not cursorEnabled then
				distorg = memory.getfloat(CCamera + 0xD4)
			elseif cursorEnabled then
				retCameraDistance(distorg)
			end
			cursorEnabled = not cursorEnabled
			showCursor(cursorEnabled)

			while isKeyDown(keyToggle) do wait(80) end
		end
	end
	if ini.settings.wh and not isPauseMenuActive() then
		for i = 0, sampGetMaxPlayerId() do
			if sampIsPlayerConnected(i) then
				local result, cped = sampGetCharHandleBySampPlayerId(i)
				local color = sampGetPlayerColor(i)
				local aa, rr, gg, bb = explode_argb(color)
				local color = join_argb(255, rr, gg, bb)
                local r25_4 = math.min(sampGetPlayerHealth(i), 100)
                local r26_4 = math.min(sampGetPlayerArmor(i), 100)
				if ini.settings.whesq then
					if doesCharExist(cped) and isCharOnScreen(cped) then
						local t = {3, 4, 5, 51, 52, 41, 42, 31, 32, 33, 21, 22, 23, 2}
						for v = 1, #t do
							pos1X, pos1Y, pos1Z = getBodyPartCoordinates(t[v], cped)
							pos2X, pos2Y, pos2Z = getBodyPartCoordinates(t[v] + 1, cped)
							pos1, pos2 = convert3DCoordsToScreen(pos1X, pos1Y, pos1Z)
							pos3, pos4 = convert3DCoordsToScreen(pos2X, pos2Y, pos2Z)
							renderDrawLine(pos1, pos2, pos3, pos4, 1, color)
						end
						for v = 4, 5 do
							pos2X, pos2Y, pos2Z = getBodyPartCoordinates(v * 10 + 1, cped)
							pos3, pos4 = convert3DCoordsToScreen(pos2X, pos2Y, pos2Z)
							renderDrawLine(pos1, pos2, pos3, pos4, 1, color)
						end
						local t = {53, 43, 24, 34, 6}
						for v = 1, #t do
							posX, posY, posZ = getBodyPartCoordinates(t[v], cped)
							pos1, pos2 = convert3DCoordsToScreen(posX, posY, posZ)
						end
					end
				end
			end
		end
	end
	if not isGamePaused() then
			g_dwSAMP_Addr = getSampDll() -- Проверяем что SAMP загружен
			g_SAMP = getSampInfoStruct() -- Проверяем что структуры SAMP загружены
			g_Players = getSampPlayerPoolStruct() -- Проверяем что структура игроков загружена
			local stopThrd = false -- Как реализовать оператор continue в Lua на адекватном уровне я не ебу, поэтому здесь костыли
			-- don't run during certain samp events

			if ini.settings.wh and stopThrd == false then
				-- for tracking player states as we iterate through
				local isPedESPCollided = {}
				local isPedESPStairStacked = {}
				-- alignment settings
				local ESP_tag_player_pixelOffsetY = -10
				local ESP_tag_player_D3DBox_pixelOffsetX = -0.5
				local ESP_tag_player_D3DBox_pixelOffsetY = -0.5
				local ESP_tag_player_posOffsetZ = 1.0
				local ESP_tag_player_espHeight = 20.0
				-- trash values to use during iterations
				local vh, va
				local iGTAID, iGTAID_Inner, selfGTAID;
				local iterPosition, ourPosition -- ourPosMinusIter не потребовалось, заменено на getDistanceBetweenCoords3d
				local poss, screenposs
				-- get our info
				if isCharInAnyCar(PLAYER_PED) then
					-- RC Vehicle Fix (Not showing names of recently streamed in players
					-- while being in a RC Vehicle)
					local carX, carY, carZ = getCarCoordinates(storeCarCharIsInNoSave(PLAYER_PED))
					ourPosition = {x = carX, y = carY, z = carZ}
				else
					local charX, charY, charZ = getCharCoordinates(PLAYER_PED)
					ourPosition = {x = charX, y = charY, z = charZ}
				end
				-- get our scripting ID so we can exclude ourself
				selfGTAID = 1
				-- get initial variables for peds streamed in
				local peds = getAllChars()
				for iterpos, iterPed in pairs(peds) do
					-- get player id
					iGTAID = iterpos
					-- ignore if it's us
					if not (iGTAID == selfGTAID) then
						-- RC Vehicle fix (not showing names of players in RC vehicles)
						if isCharInAnyCar(iterPed) then
							local veh = storeCarCharIsInNoSave(iterPed)
							if isVehicleClassSmall(veh) and getDriverOfCar(veh) == iterPed then
								local carX, carY, carZ = getCarCoordinates(veh)
								iterPosition = {fX = carX, fY = carY, fZ = carZ}
							else
								local charX, charY, charZ = getCharCoordinatesFixed(iterPed) -- Пофикшено: персонаж в ТС, и Lua по неизвестной причине возвращает координаты ТС вместо координат персонажа
								iterPosition = {fX = charX, fY = charY, fZ = charZ}
							end
						else
							local charX, charY, charZ = getCharCoordinates(iterPed)
							iterPosition = {fX = charX, fY = charY, fZ = charZ}
						end
						-- check if it's farther than set.player_tags_dist
						if getDistanceBetweenCoords3d(ourPosition.x, ourPosition.y, ourPosition.z, iterPosition.fX, iterPosition.fY, iterPosition.fZ) < set.player_tags_dist then
							g_playerTagInfo[iGTAID].isPastMaxDistance = false
							-- get the player position in 2D
							poss = {x = 0.0, y = 0.0, z = 0.0}
							poss.x = iterPosition.fX
							poss.y = iterPosition.fY
							poss.z = iterPosition.fZ + ESP_tag_player_posOffsetZ
							screenposs = {x = 0.0, y = 0.0, z = 0.0}
							screenposs.x, screenposs.y, screenposs.z = CalcScreenCoors(poss.x, poss.y, poss.z)
							-- check if the iter is culled or not
							if screenposs.z > 1.0 then
								-- global, set ESP position for tagOffsetY use
								g_playerTagInfo[iGTAID].tagPosition.fX = screenposs.x
								g_playerTagInfo[iGTAID].tagPosition.fY = screenposs.y
								g_playerTagInfo[iGTAID].tagPosition.fZ = screenposs.z
							else
								g_playerTagInfo[iGTAID].tagOffsetY = 0.0
								g_playerTagInfo[iGTAID].isPastMaxDistance = true
							end
						else
							g_playerTagInfo[iGTAID].isPastMaxDistance = true
						end
					end
				end
				-- remove staircase problem
				for iterpos, iterPed in pairs(peds) do
					-- get player id
					iGTAID = iterpos
					-- ignore if it's us
					if not (iGTAID == selfGTAID) then
						-- filter out "ok" ESP
						if not (g_playerTagInfo[iGTAID].isPastMaxDistance or not g_playerTagInfo[iGTAID].isStairStacked and g_playerTagInfo[iGTAID].tagOffsetY < 40.0) then
							-- detect stair stacking per frame if ESP isn't already stair stacked
							if not g_playerTagInfo[iGTAID].isStairStacked then
								for iterInnerpos, iterInnerPed in pairs(peds) do
									-- get player id
									iGTAID_Inner = iterInnerpos
									-- ignore if it's us or isPastMaxDistance
									if not (g_playerTagInfo[iGTAID_Inner].isPastMaxDistance or iGTAID_Inner == iGTAID) then
										-- test to see who comes out on top
										if math.abs(g_playerTagInfo[iGTAID].tagPosition.fX - g_playerTagInfo[iGTAID_Inner].tagPosition.fX) <= 100.0 and math.abs((g_playerTagInfo[iGTAID].tagPosition.fY - (g_playerTagInfo[iGTAID].tagOffsetY / 2.0)) - (g_playerTagInfo[iGTAID_Inner].tagPosition.fY - g_playerTagInfo[iGTAID_Inner].tagOffsetY)) <= ESP_tag_player_espHeight then
											isPedESPStairStacked[iGTAID] = false
										end
									end -- 3rd continue
								end
								-- setup stair stack variables needed to un stack the ESP
								if isPedESPStairStacked[iGTAID] then
									g_playerTagInfo[iGTAID].isStairStacked = true
									g_playerTagInfo[iGTAID].stairStackedOffset = g_playerTagInfo[iGTAID].tagOffsetY / 2.0
								end
							end -- end inner while - detect stair stacking
							-- lower the offsets for stair stacked ESP
							-- and turn off stack status of ESP that reaches the "available" offset
							if g_playerTagInfo[iGTAID].isStairStacked then
								g_playerTagInfo[iGTAID].tagOffsetY = g_playerTagInfo[iGTAID].tagOffsetY - 5.0
								g_playerTagInfo[iGTAID].stairStackedOffset = g_playerTagInfo[iGTAID].stairStackedOffset - 5.0
								if g_playerTagInfo[iGTAID].stairStackedOffset < 5.0 then
									g_playerTagInfo[iGTAID].stairStackedOffset = 0.0
									g_playerTagInfo[iGTAID].isStairStacked = false
								end
							end
						end -- 2nd continue
					end -- 1st continue
				end -- end outer while - remove staircase problem
				-- detect & adjust for ESP collisions
				for iterpos, iterPed in pairs(peds) do
					-- get player id
					iGTAID = iterpos
					-- we isPastMaxDistance or stairstacked, move along
					if not (g_playerTagInfo[iGTAID].isPastMaxDistance or g_playerTagInfo[iGTAID].isStairStacked) then
						for iterInnerpos, iterInnerPed in pairs(peds) do
							-- get player id
							iGTAID_Inner = iterInnerpos
							-- filter out isPastMaxDistance, stairstacked, and same Ped
							if not (g_playerTagInfo[iGTAID].isPastMaxDistance or g_playerTagInfo[iGTAID_Inner].isStairStacked or iGTAID == iGTAID_Inner) then
								-- player is within range, figure out if there's collision
								if math.abs(g_playerTagInfo[iGTAID].tagPosition.fX - g_playerTagInfo[iGTAID_Inner].tagPosition.fX) <= 100.0 and math.abs((g_playerTagInfo[iGTAID].tagPosition.fY - g_playerTagInfo[iGTAID].tagOffsetY) - (g_playerTagInfo[iGTAID_Inner].tagPosition.fY - g_playerTagInfo[iGTAID_Inner].tagOffsetY)) <= ESP_tag_player_espHeight then
									-- collision, figure out who gets to stay
									if g_playerTagInfo[iGTAID].tagPosition.fZ < g_playerTagInfo[iGTAID_Inner].tagPosition.fZ then
										-- playerID "g_pTI_i" is farther, it should move up
										g_playerTagInfo[iGTAID_Inner].tagOffsetY = g_playerTagInfo[iGTAID_Inner].tagOffsetY + 5.0
										isPedESPCollided[iGTAID_Inner] = true
									elseif g_playerTagInfo[iGTAID].tagPosition.fZ > g_playerTagInfo[iGTAID_Inner].tagPosition.fZ then
										-- playerID "i" is farther, it should move up
										-- we should only need normal upward movement here
										g_playerTagInfo[iGTAID].tagOffsetY = g_playerTagInfo[iGTAID].tagOffsetY + 5.0
										isPedESPCollided[iGTAID] = true
									else
										-- both playerIDs are the same position @_@ so prefer the lower ID#
										if iGTAID < iGTAID_Inner then
											g_playerTagInfo[iGTAID_Inner].tagOffsetY = g_playerTagInfo[iGTAID_Inner].tagOffsetY + 5.0
											isPedESPCollided[iGTAID_Inner] = true
										else
											g_playerTagInfo[iGTAID].tagOffsetY = g_playerTagInfo[iGTAID].tagOffsetY + 5.0
											isPedESPCollided[iGTAID] = true
										end
									end
								end
								-- are we jigglin?  everybody likes ta jiggle.
								if math.abs(g_playerTagInfo[iGTAID].tagPosition.fX - g_playerTagInfo[iGTAID_Inner].tagPosition.fX) <= 100.0 and math.abs((g_playerTagInfo[iGTAID].tagPosition.fY - g_playerTagInfo[iGTAID].tagOffsetY) - (g_playerTagInfo[iGTAID_Inner].tagPosition.fY - g_playerTagInfo[iGTAID_Inner].tagOffsetY)) - 5.0 <= ESP_tag_player_espHeight then
									if g_playerTagInfo[iGTAID].tagPosition.fZ < g_playerTagInfo[iGTAID_Inner].tagPosition.fZ then
										isPedESPCollided[iGTAID_Inner] = true
									else
										isPedESPCollided[iGTAID] = true
									end
								end
							end -- 2nd continue
						end -- end inner while
						-- return tagOffsetY to zero if needed
						if not isPedESPCollided[iGTAID] then
							if g_playerTagInfo[iGTAID].tagOffsetY >= 5.0 then
								g_playerTagInfo[iGTAID].tagOffsetY = g_playerTagInfo[iGTAID].tagOffsetY - 5.0
							else
								g_playerTagInfo[iGTAID].tagOffsetY = 0.0
							end
						end
					end -- 1st continue
				end -- end outer while
				-- start render ESP tags
				local h, playerBaseY -- w не понадобилось (не используется нигде, даже в оригинальном сурсе)
				for iterpos, iterPed in pairs(peds) do
					-- get player id
					iGTAID = iterpos
					-- ignore if isPastMaxDistance or if it's us
					if not (g_playerTagInfo[iGTAID].isPastMaxDistance or iGTAID == selfGTAID) then
						playerBaseY = g_playerTagInfo[iGTAID].tagPosition.fY - g_playerTagInfo[iGTAID].tagOffsetY + ESP_tag_player_pixelOffsetY
						local iSAMPID
						if g_Players ~= -1 then
							if isSampfuncsLoaded() then
								local result, tempId = sampGetPlayerIdByCharHandle(iterPed)
								if result then
									iSAMPID = tempId
								end
							end
						end
						-- get Ped health
						-- works in single player, but SAMP maintains its own player health
						--vh = iterPed->GetHealth();
						-- get samp health
						local brk = false -- Очередные костыли из-за оператора continue
						if g_Players ~= -1 then
							if iSAMPID ~= nil then
								vh = sampGetPlayerHealth(iSAMPID)
								va = sampGetPlayerArmor(iSAMPID)
							else
								-- SA-MP running, but was not a remote player
								brk = true
							end
						else
							vh = getCharHealth(iterPed)
							va = getCharArmour(iterPed)
						end
						if brk == false then
							local color = join_argb(75, 0, 200, 0)
							if vh > 100.0 then vh = 100.0 end
							if vh < 100.0 and vh > 60.0 then
								color = join_argb(111, 0, 200, 0)
							elseif vh < 60.0 and vh > 20.0 then
								color = join_argb(111, 200, 200, 0)
							elseif vh < 20.0 and vh > 0.0 then
								color = join_argb(111, 200, 0, 0)
							end
							renderDrawBox(g_playerTagInfo[iGTAID].tagPosition.fX + ESP_tag_player_D3DBox_pixelOffsetX, playerBaseY + ESP_tag_player_D3DBox_pixelOffsetY, 100.0, 10.0, join_argb(111, 0, 0, 0))
							renderDrawBox(g_playerTagInfo[iGTAID].tagPosition.fX + 1.0 + ESP_tag_player_D3DBox_pixelOffsetX, playerBaseY + 1.0 + ESP_tag_player_D3DBox_pixelOffsetY, vh - 2.0, 8.0, color)
							if va > 0.0 then
								if va > 100.0 then va = 100.0 end
								va = va / 1.0
								renderDrawBox(g_playerTagInfo[iGTAID].tagPosition.fX + ESP_tag_player_D3DBox_pixelOffsetX, playerBaseY + ESP_tag_player_D3DBox_pixelOffsetY, va - 1.0, 10.0, join_argb(111, 0, 0, 0))
								renderDrawBox(g_playerTagInfo[iGTAID].tagPosition.fX + 1.0 + ESP_tag_player_D3DBox_pixelOffsetX, playerBaseY + 1.0 + ESP_tag_player_D3DBox_pixelOffsetY, va - 2.0, 8.0, join_argb(111, 220, 220, 220))
							end
							-- this should also calculate the anti-aliasing top edge somehow
							h = renderGetFontDrawHeight(pD3DFontFixedSmall) + 1
							-- already check if player is ok before
							-- so now we only need to check if samp is running
							if g_Players == -1 then
								PrintShadow(pD3DFontFixedSmall, "H: " .. getCharHealth(iterPed) .. ", A: " .. getCharArmour(iterPed), g_playerTagInfo[iGTAID].tagPosition.fX + 8.0, playerBaseY - h + 10.0, join_argb(130, 0xFF, 0x6A, 0))
							else
								if isSampfuncsLoaded() then
									PrintShadow(pD3DFontFixedSmall, "H: " .. sampGetPlayerHealth(iSAMPID) .. ", A: " .. sampGetPlayerArmor(iSAMPID), g_playerTagInfo[iGTAID].tagPosition.fX + 8.0, playerBaseY - h + 10.0, join_argb(130, 0xFF, 0x6A, 0))
									-- render the main nametag last so it's on top
									-- this should calculate the anti-aliasing top edge somehow
									h = renderGetFontDrawHeight(pD3DFont_sampStuff) - 1
									local _, r, g, b = explode_argb(sampGetPlayerColor(iSAMPID))
									PrintShadow(pD3DFont_sampStuff, sampGetPlayerNickname(iSAMPID) .. " (" .. iSAMPID .. ")", g_playerTagInfo[iGTAID].tagPosition.fX, playerBaseY - h, join_argb(0xDD, r, g, b))
									-- w = renderGetFontDrawHeight(pD3DFont_sampStuff)
									if sampIsPlayerPaused(iSAMPID) then
										renderFontDrawText(pD3DFontFixedSmall, "AFK", g_playerTagInfo[iGTAID].tagPosition.fX + ESP_tag_player_D3DBox_pixelOffsetX + 100.0 + 2.0, playerBaseY + ESP_tag_player_D3DBox_pixelOffsetY - 2.0, join_argb(0xFF, 0xF0, 0xF0, 0xF0))
									end
								end
							end
						end
					end
				end -- end render ESP tags
			end
		end
    if cursorEnabled then
		local mode = sampGetCursorMode()
		if mode == 0 then
		showCursor(true)
		end
		local sx, sy = getCursorPos()
		local sw, sh = getScreenResolution()
		if sx >= 0 and sy >= 0 and sx < sw and sy < sh then
			local posX, posY, posZ = convertScreenCoordsToWorld3D(sx, sy, 200)
			local camX, camY, camZ = getActiveCameraCoordinates()
			local result, colpoint = processLineOfSight(camX, camY, camZ, posX, posY, posZ, true, false, false, false, false, true, false)
			if result and colpoint.entity ~= 0 then
				local normal = colpoint.normal
				local pos = Vector3D(colpoint.pos[1], colpoint.pos[2], colpoint.pos[3]) - (Vector3D(normal[1], normal[2], normal[3]) * 0.1)
				local zOffset = 300
				if normal[3] >= 0.5 then zOffset = 1 end
				local result, colpoint2 = processLineOfSight(pos.x, pos.y, pos.z + zOffset, pos.x, pos.y, pos.z - 0.3, true, false, false, false, false, true, false)
				if result then
					pos = Vector3D(colpoint2.pos[1], colpoint2.pos[2], colpoint2.pos[3] + 1)

					local curX, curY, curZ  = getCharCoordinates(playerPed)
					local dist              = tonumber(string.format("%02.0f", getDistanceBetweenCoords3d(curX, curY, curZ, pos.x, pos.y, curZ)))
					local hoffs             = renderGetFontDrawHeight(font)

					sy = sy - 2
					sx = sx - 2
					if tonumber(string.format("%02.0f", dist)) >= 51 then 
						renderFontDrawText(font, "50m", sx, sy - hoffs, 0xEEFF0000)
						dist = 50
						setCameraDistance(dist)
					else
						renderFontDrawText(font, string.format("%02.0fm", dist), sx, sy - hoffs, 0xEEEEEEEE)
					end
					drawCircleIn3d(curX, curY, curZ - 1, dist, 0xFFff004d, 2, 40) 
					setCameraDistance(dist + 50)
					if isKeyDown(keyApply) then
						if tonumber(string.format("%02.0f", dist)) >= 51 then
							dist = 50
						end
						sampSendChat("/respawncars "..string.format("%02.0f", dist))
						-- while isKeyDown(keyApply) do wait(0) end
						cursorEnabled = not cursorEnabled
						retCameraDistance(distorg)
						showCursor(cursorEnabled)
					end
				end
			end
		end
    end
	wait(0)
	end
end

regac = {
	
}

function sampev.onServerMessage(color, text)
	local testt = string.match(text, "Bienvenido{FFB319} (%a-)%s(%a-){FFFFFF}, (.-) {FFB319}(%d-){FFFFFF}/{FFB319}(%d-){FFFFFF}/{FFB319}(%d-){FFFFFF}.")
	if testt and ini.settings.susreg then
		maxPlayerOnline = sampGetMaxPlayerId(false)
		for id = 0, maxPlayerOnline, 1 do
			if sampIsPlayerConnected(id) then
				name = sampGetPlayerNickname(id)
				for k, v in pairs(notifytable) do
					if name:lower() == v.name then
						sampAddChatMessage(tag.."Usuario sospechoso: {FFFFFF}{34eb46}" .. name .. " (" .. id .. ")" .. "{FFFFFF} se encuentra conectado.", 16777215)
					end
				end
			end
		end
	end
	local nom,ape,razon = text:match("%[ADMLOG%] {FFFFFF}(%a-)%s(%a-) ha sido expulsado por (.+).") 
	if nom and ape and razon then
		nombre = nom .. "_" .. ape
		local yaesta = false
		for k, v in pairs(regac) do
			if v.name:lower() == nombre:lower() then
				yaesta = true
			end
		end
		if not yaesta then 
		table.insert(regac, { name = nombre, razon = razon, fecha = os.time(), full = text:gsub('{......}','') })
		end
	end
	if text:find("Centro de Ayuda: Staff") and aggcmdchat then
		return false 
	end
	
	if text:find("Moderador (%d-)(.+)") and aggcmdchat then
		for comand in string.gmatch(text,"/%a+") do
			if comand ~= "/A" then
			comand = comand:gsub("/","")
			commaf.v = commaf.v .. comand ..'\n'
			end
		end
		tiemporev = os.time() + 2
		return false
	end
end

local fonty = nil
local fontys = nil
function imgui.BeforeDrawFrame()
    if fa_font2 == nil then
        local font_config = imgui.ImFontConfig()
        font_config.MergeMode = true

        fa_font2 = imgui.GetIO().Fonts:AddFontFromFileTTF('moonloader/resource/fonts/fa-solid-900.ttf', 20.0, font_config, fa_glyph_ranges)
    end
    if fa_font == nil then
        local font_config = imgui.ImFontConfig()
        font_config.MergeMode = true

        fa_font = imgui.GetIO().Fonts:AddFontFromFileTTF('moonloader/resource/fonts/fa-solid-900.ttf', 15.0, font_config, fa_glyph_ranges)
    end
	if fonty == nil then
    fonty = imgui.GetIO().Fonts:AddFontFromFileTTF(getFolderPath(20) .. "\\trebucbd.ttf", 19, nil, imgui.GetIO().Fonts:GetGlyphRangesCyrillic())
	end
	if fontyc == nil then
    fontyc = imgui.GetIO().Fonts:AddFontFromFileTTF(getFolderPath(20) .. "\\arialbd.ttf", 15, nil, imgui.GetIO().Fonts:GetGlyphRangesCyrillic())
	end
	if fontys == nil then
    fontys = imgui.GetIO().Fonts:AddFontFromFileTTF(getFolderPath(20) .. "\\arialbd.ttf", 10, nil, imgui.GetIO().Fonts:GetGlyphRangesCyrillic())
	end
end

local tab = imgui.ImInt(1)
local tabs = {
    fa.ICON_FA_CALCULATOR..u8' Calculadora',
    fa.ICON_FA_LIST_ALT..u8' Sanciones',
    fa.ICON_FA_USER_SECRET..u8' Sospechosos',
    fa.ICON_FA_SITEMAP..u8' IP Checker',
    fa.ICON_FA_TAPE..u8' Anti Fail',
    fa.ICON_FA_USER_SHIELD..u8' Registro AC',
    fa.ICON_FA_WRENCH..u8' Opciones',
    fa.ICON_FA_FILE_ALT..u8' Changelog',
}

function split(string, separ)
    local result = {}
    for str in string.gmatch(string, "([^" .. separ .. "]+)") do
        table.insert(result, str)
    end
    return result
end

function imgui.OnDrawFrame()
	style2()
    local X, Y = getScreenResolution()
	local myid = select(2, sampGetPlayerIdByCharHandle(playerPed))
	local mynick = sampGetPlayerNickname(myid)
	if mainwindow.v then
    imgui.SetNextWindowSize(imgui.ImVec2(380, 340), imgui.Cond.FirstUseEver)
    imgui.SetNextWindowPos(imgui.ImVec2(X / 2 - 55, Y / 2), imgui.Cond.FirstUseEver, imgui.ImVec2(0.5, 0.5))
    imgui.Begin('#window'..mynick, mainwindow, imgui.WindowFlags.NoCollapse + imgui.WindowFlags.NoResize + imgui.WindowFlags.NoScrollbar + imgui.WindowFlags.NoTitleBar)
		imgui.PushFont(fonty)
		imgui.TextColoredRGB(u8'ModTools | Bienvenido '..mynick)
		imgui.PopFont()
		imgui.SetCursorPos(imgui.ImVec2(353, 4))
		if imgui.OutlineButton(u8' X ', nil) then
			imgui.ShowCursor = false
			mainwindow.v = not mainwindow.v
		end
		imgui.SetCursorPos(imgui.ImVec2(0, 45))

        if imgui.CustomMenu(tabs, tab, imgui.ImVec2(100, 30)) then end
		imgui.SetCursorPos(imgui.ImVec2(5, 300))
		imgui.TextColoredRGB(u8'Creado por:\n{99068d}Xaero ')
        imgui.SetCursorPos(imgui.ImVec2(110, 35))
        imgui.BeginChild('##main', imgui.ImVec2(269, 298), true)
            if tab.v == 1 then
				imgui.PushFont(fontyc)
				imgui.TextColoredRGB(u8' Calculadora de Sanciones  | ')
				imgui.SameLine()
				imgui.TextColoredRGB(u8'? ')
				imgui.PopFont()
				imgui.Hint('hintSecret',fa.ICON_FA_SEARCH..' Ayuda: \n Introduce el nivel del usuario y las infraciones cometidas\n con formado "XX,YY,ZZ" en los lugares indicados.\n \nEjemplo:\n Nivel: 10\n Infraciones: DM,NRE,NRA', 0.1)
				imgui.SetCursorPosX(35)
				if imgui.BetterInput("##nivel", u8("  Nivel"), nivelc, nil, nil, 200) then
					nivelc.v = nivelc.v:gsub("%D", "")
				end
				imgui.SetCursorPosX(35)
				if imgui.BetterInput("##infra", u8("  Infracciones"), infracionesc, nil, nil, 200) then
					infracionesc.v = infracionesc.v:gsub("[^%a%d,]", "")
				end

				imgui.SetCursorPosX(105)
				imgui.SetCursorPosY(115)
				if imgui.OutlineButton(u8"Calcular!",  nil) then
					if nivelc.v == "" or infracionesc.v == "" then
						sampAddChatMessage("{2a2cad}[ Calculador | ModTools ]: {FFFFFF}No has calculado nada.")
					
					elseif tonumber(nivelc.v) == 0 then
						sampAddChatMessage("{2a2cad}[ Calculador | ModTools ]: {FFFFFF}Ingresaste un nivel de usuario invalido. No se ha calculado nada.")
					else
					local nvl = tonumber(nivelc.v)
					local infracs = split(infracionesc.v, ",")
					local tfinal = 0
					local tbfinal = 0
					local binfrac = false
					local Xinfrac = false
					local exinfrac = false
					for k, v in ipairs(infracs) do
						v = v:upper()
						if v == "DM" then
							if 1 <= nvl and nvl <= 9 then
								tfinal = tfinal + 40
							elseif 10 <= nvl and nvl <= 20 then
								tfinal = tfinal + 60
							elseif nvl >= 21 then
								tfinal = tfinal + 80
							end
						elseif v == "HK" then
							if nvl >= 1 then
								tfinal = tfinal + 50
							end
						elseif v == "TK" then
							if 1 <= nvl and nvl <= 9 then
								tfinal = tfinal + 20
							elseif 10 <= nvl and nvl <= 20 then
								tfinal = tfinal + 40
							elseif nvl >= 21 then
								tfinal = tfinal + 100
							end
						elseif v == "AHQ" then
							if nvl >= 3 then
								tfinal = tfinal + 60
							end
						elseif v == "SK" then
							if 1 <= nvl and nvl <= 5 then
								tfinal = tfinal + 15
							elseif 6 <= nvl and nvl <= 12 then
								tfinal = tfinal + 50
							elseif 13 <= nvl and nvl <= 20 then
								tfinal = tfinal + 60
							elseif nvl >= 21 then
								tfinal = tfinal + 90
							end
						elseif v == "RK" then
							if 1 <= nvl and nvl <= 10 then
								tfinal = tfinal + 20
							elseif 11 <= nvl and nvl <= 20 then
								tfinal = tfinal + 40
							elseif nvl >= 21 then
								tfinal = tfinal + 60
							end
						elseif v == "DB" then
							if 1 <= nvl and nvl <= 5 then
								tfinal = tfinal + 15
							elseif 6 <= nvl and nvl <= 12 then
								tfinal = tfinal + 30
							elseif nvl >= 13 then
								tfinal = tfinal + 45
							end
						elseif v == "ARAD" then
							if 1 <= nvl and nvl <= 6 then
								tfinal = tfinal + 15
							elseif 7 <= nvl and nvl <= 30 then
								tfinal = tfinal + 30
							elseif 15 <= nvl and nvl <= 20 then
								tfinal = tfinal + 35
							elseif nvl >= 21 then
								tfinal = tfinal + 40
							end
							sampAddChatMessage("{2a2cad}[ Calculador | ModTools ]: {FF0000}Abuso \"leve\" de la radio = Advertencia.", -1)
						elseif v == "NRE" then
							if 1 <= nvl and nvl <= 6 then
								exinfrac = true
								sampAddChatMessage("{2a2cad}[ Calculador | ModTools ]: {FF0000}Jugador nivel 6 o inferior que comete NRE = Advertencia.", -1)
							elseif 7 <= nvl and nvl <= 10 then
								tfinal = tfinal + 20
							elseif 11 <= nvl and nvl <= 15 then
								tfinal = tfinal + 40
							elseif 16 <= nvl and nvl <= 20 then
								tfinal = tfinal + 60
							elseif nvl >= 21 then
								tfinal = tfinal + 80
							end
						  elseif v == "NRA" then
							if 1 <= nvl and nvl <= 5 then
							  tfinal = tfinal + 15
							elseif 6 <= nvl and nvl <= 12 then
							  tfinal = tfinal + 30
							elseif 13 <= nvl and nvl <= 20 then
							  tfinal = tfinal + 60
							elseif nvl >= 21 then
							  tfinal = tfinal + 90
							end
						  elseif v == "ASIS" then
							if 1 <= nvl and nvl <= 6 then
							  tfinal = tfinal + 30
							elseif 7 <= nvl and nvl <= 12 then
							  tfinal = tfinal + 40
							elseif 13 <= nvl and nvl <= 20 then
							  tfinal = tfinal + 60
							elseif nvl >= 21 then
							  tfinal = tfinal + 100
							end
						  elseif v == "CBUG" then
							if 1 <= nvl and nvl <= 15 then
							  tfinal = tfinal + 30
							elseif 16 <= nvl and nvl <= 20 then
							  tfinal = tfinal + 75
							elseif nvl >= 21 then
							  binfrac = true
							  tbfinal = 24
							  sampAddChatMessage("{2a2cad}[ Calculador | ModTools ]: {FF0000}Jugador nivel +21 que comete CBUG = Bloqueo temporal 24 horas.", -1)
							end
						  elseif v == "BA" then
							if 1 <= nvl and nvl <= 7 then
							  tfinal = tfinal + 20
							elseif 8 <= nvl and nvl <= 14 then
							  tfinal = tfinal + 40
							elseif nvl >= 15 then
							  tfinal = tfinal + 60
							end
						  sampAddChatMessage("{2a2cad}[ Calculador | ModTools ]: {FF0000}Casos graves de BA (Dinero,Materiales,Armas, etc) = Bloqueo permanente.", -1)
						  elseif v == "MG" then
							if 1 <= nvl and nvl <= 5 then
							  sampAddChatMessage("{2a2cad}[ Calculador | ModTools ]: {FF0000}Jugador nivel 5 o inferior que comete MG = Advertencia.", -1)
							elseif 6 <= nvl and nvl <= 12 then
							  tfinal = tfinal + 40
							elseif 13 <= nvl and nvl <= 20 then
							  tfinal = tfinal + 60
							elseif nvl >= 21 then
							  tfinal = tfinal + 100
							end
						  elseif v == "PG2" then
							if 1 <= nvl and nvl <= 5 then
							  tfinal = tfinal + 30
							elseif 6 <= nvl and nvl <= 9 then
							  tfinal = tfinal + 60
							elseif 10 <= nvl and nvl <= 19 then
							  binfrac = true
							  tbfinal = tbfinal + 6
							  sampAddChatMessage("{2a2cad}[ Calculador | ModTools ]: {FF0000}Nivel +10 que comete PG2 = Bloqueo temporal 6 horas, en caso de preferir sancion o hacerlo aproposito, 12 horas", -1)
							elseif 20 <= nvl and nvl <= 29 then
							  binfrac = true
							  tbfinal = tbfinal + 16
							  sampAddChatMessage("{2a2cad}[ Calculador | ModTools ]: {FF0000}Nivel +20 que comete PG2 = Bloqueo temporal 16 horas, en caso de preferir sancion o hacerlo aproposito, 32 horas", -1)
							elseif nvl >= 30 then
							  binfrac = true
							  tbfinal = tbfinal + 48
							  sampAddChatMessage("{2a2cad}[ Calculador | ModTools ]: {FF0000}Nivel +30 que comete PG2 = Bloqueo temporal 48 horas, en caso de preferir sancion o hacerlo aproposito, 96 horas.", -1)
							end
						  elseif v == "CK" then
							if nvl >= 1 then
							  tfinal = tfinal + 40
							  sampAddChatMessage("{2a2cad}[ Calculador | ModTools ]: {FF0000}Jugador nivel +1 que comete CK = 40 minutos, CK masivo = 60 minutos.", -1)
							end
						  elseif v == "TOX" then
							if nvl >= 1 then
							  tfinal = tfinal + 30
							  sampAddChatMessage("{2a2cad}[ Calculador | ModTools ]: {FF0000}Palabra poco ofensiva (ez,zzz,feo,bobo,etc) = 30 minutos.", -1)
							end
						  elseif v == "IOOC" then
							if nvl >= 1 then
							  tfinal = tfinal + 60
							  sampAddChatMessage("{2a2cad}[ Calculador | ModTools ]: {FF0000}Insultos medianamente ofensivos (tonto, bruto, puto, etc) = 60 minutos.", -1)
							  sampAddChatMessage("{2a2cad}[ Calculador | ModTools ]: {FF0000}Insultos grave/amenazante/humillante = Bloqueo temporal de 2 a 5 dias.", -1)
							end
						  elseif v == "TROLL" then
							if 1 <= nvl and nvl <= 10 then
							  sampAddChatMessage("{2a2cad}[ Calculador | ModTools ]: {FF0000}Ban permanente en caso de trolear demasiado, caso contrario = Advertencia verbal.", -1)
							  tfinal = tfinal + 20
							elseif 11 <= nvl and nvl <= 24 then
							  tfinal = tfinal + 40
							elseif 25 <= nvl and nvl <= 29 then
							  tfinal = tfinal + 60
							elseif nvl >= 30 then
							  tfinal = tfinal + 80
							end
						  elseif v == "BD" then
							if 1 <= nvl and nvl <= 5 then
							  exinfrac = true
							  sampAddChatMessage("{2a2cad}[ Calculador | ModTools ]: {FF0000}Jugador nivel 5 o inferior que comete BD = Advertencia.", -1)
							elseif 6 <= nvl and nvl <= 12 then
							  tfinal = tfinal + 30
							elseif nvl >= 13 then
							  tfinal = tfinal + 60
							end
						  elseif v == "NIP" then
							if 1 <= nvl and nvl <= 5 then
							  tfinal = tfinal + 40
							elseif 6 <= nvl and nvl <= 12 then
							  tfinal = tfinal + 50
							elseif 13 <= nvl and nvl <= 19 then
							  tfinal = tfinal + 60
							elseif 20 <= nvl and nvl <= 25 then
							  tfinal = tfinal + 80
							elseif nvl >= 26 then
							  binfrac = true
							  tbfinal = tbfinal+ 72
							  sampAddChatMessage("{2a2cad}[ Calculador | ModTools ]: {FF0000}Jugador nivel +26 que comete NIP = Bloqueo temporal de 72 a 120 horas.", -1)
							end
						  elseif v == "INS" then
							if 1 <= nvl and nvl <= 6 then
							  tfinal = tfinal + 50
							elseif nvl >= 7 then
							  tfinal = tfinal + 70
							end
						  elseif v == "PG" then
							if 1 <= nvl and nvl <= 5 then
							  exinfrac = true
							  sampAddChatMessage("{2a2cad}[ Calculador | ModTools ]: {FF0000}Jugador nivel 5 o inferior que comete PG = Advertencia.", -1)
							elseif 6 <= nvl and nvl <= 12 then
							  tfinal = tfinal + 40
							elseif 13 <= nvl and nvl <= 20 then
							  tfinal = tfinal + 60
							elseif nvl >= 21 then
							tfinal = tfinal + 90
							end
						  elseif v == "NVVPJ" then
							if 1 <= nvl and nvl <= 5 then
							  exinfrac = true
							  sampAddChatMessage("{2a2cad}[ Calculador | ModTools ]: {FF0000}Jugador nivel 5 o inferior que comete NVVPJ = Advertencia.", -1)
							elseif 6 <= nvl and nvl <= 12 then
							  tfinal = tfinal + 30
							elseif 13 <= nvl and nvl <= 20 then
							  tfinal = tfinal + 50
							elseif nvl >= 21 then
							  tfinal = tfinal + 90
							end
						  elseif v == "CJ" then
							if nvl >= 6 then
							  tfinal = tfinal + 15
							end
						  elseif v == "AIN" then
							if 1 <= nvl and nvl <= 6 then
							  tfinal = tfinal + 15
							elseif 7 <= nvl and nvl <= 15 then
							  tfinal = tfinal + 30
							elseif 16 <= nvl and nvl <= 20 then
							  tfinal = tfinal + 45
							elseif nvl >= 21 then
							  tfinal = tfinal + 60
							end
						  elseif v == "FLOOD" then
							if 1 <= nvl and nvl <= 7 then
							  exinfrac = true
							  sampAddChatMessage("{2a2cad}[ Calculador | ModTools ]: {FF0000}Jugador nivel 7 o inferior que comete FLOOD = Advertencia.", -1)
							elseif 8 <= nvl and nvl <= 16 then
							  tfinal = tfinal + 20
							elseif 17 <= nvl and nvl <= 20 then
							  tfinal = tfinal + 40
							elseif nvl >= 21 then
							  tfinal = tfinal + 80
							end
						  elseif v == "MUR" then
							if 1 <= nvl and nvl <= 5 then
							  exinfrac = true
							  sampAddChatMessage("{2a2cad}[ Calculador | ModTools ]: {FF0000}Jugador nivel 5 o inferior que comete MUR = Advertencia.", -1)
							elseif 6 <= nvl and nvl <= 10 then
							  tfinal = tfinal + 15
							elseif 11 <= nvl and nvl <= 20 then
							  tfinal = tfinal + 30
							elseif nvl >= 21 then
							  exinfrac = true
							  sampAddChatMessage("{2a2cad}[ Calculador | ModTools ]: {FF0000}Jugador nivel +21 que comete MUR = Bloqueo del '/re' por 15 dias, subir a #info-users.", -1)
							end
						  elseif v == "MUP" then
							if 1 <= nvl and nvl <= 5 then
							  sampAddChatMessage("{2a2cad}[ Calculador | ModTools ]: {FF0000}Jugador nivel 5 o inferior que comete MUP = Advertencia verbal.", -1)
							elseif 6 <= nvl and nvl <= 10 then
							  exinfrac = true
							  sampAddChatMessage("{2a2cad}[ Calculador | ModTools ]: {FF0000}Jugador nivel +5 que comete MUP = Advertencia.", -1)
							elseif 11 <= nvl and nvl <= 20 then
							  tfinal = tfinal + 15
							elseif nvl >= 21 then
							  exinfrac = true
							  sampAddChatMessage("{2a2cad}[ Calculador | ModTools ]: {FF0000}Jugador nivel +21 que comete MUP = Bloqueo del '/mp' por 15 dias, subir a #info-users.", -1)
							end
						  elseif v == "MVT" then
							if 1 <= nvl and nvl <= 5 then
							  sampAddChatMessage("{2a2cad}[ Calculador | ModTools ]: {FF0000}Jugador nivel 5 o inferior que comete MVT = Advertencia.", -1)
							elseif 6 <= nvl and nvl <= 10 then
							  tfinal = tfinal + 15
							elseif 11 <= nvl and nvl <= 20 then
							  tfinal = tfinal + 40
							elseif nvl >= 21 then
							  binfrac = true
							  tbfinal = tbfinal+ 24
							  sampAddChatMessage("{2a2cad}[ Calculador | ModTools ]: {FF0000}Jugador nivel +21 que comete MVT = Bloqueo temporal de 24 a 48 horas.", -1)
							end
						  elseif v == "MUA" then
							if 1 <= nvl and nvl <= 6 then
							  exinfrac = true
							  sampAddChatMessage("{2a2cad}[ Calculador | ModTools ]: {FF0000}Jugador nivel 5 o inferior que comete MUA = Advertencia.", -1)
							elseif 7 <= nvl and nvl <= 20 then
							  tfinal = tfinal + 20
							elseif nvl >= 21 then
							  tfinal = tfinal + 100
							end
						  elseif v == "MUD" then
							if nvl >= 1 then
							  sampAddChatMessage("{2a2cad}[ Calculador | ModTools ]: {FF0000}MUD = Advertencia + Bloqueo temporal de 30 - 60 minutos del /duda.", -1)
							end
						  elseif v == "MALUSO" then
							if nvl >= 1 then
							  sampAddChatMessage("{2a2cad}[ Calculador | ModTools ]: {FF0000}Mal uso del /yo, /callsign o /accesorios = Advertencia, si continua = 20 minutos.", -1)
							end
						  elseif v == "INR" then
							if 1 <= nvl and nvl <= 5 then
							  sampAddChatMessage("{2a2cad}[ Calculador | ModTools ]: {FF0000}Jugador nivel 5 o inferior que comete INR = Advertencia verbal.", -1)
							elseif 6 <= nvl and nvl <= 12 then
							  tfinal = tfinal + 30
							elseif 13 <= nvl and nvl <= 20 then
							  tfinal = tfinal + 50
							elseif nvl >= 21 then
							  tfinal = tfinal + 80
							end
						  elseif v == "MALE1" then
							if 1 <= nvl and nvl <= 6 then
							  sampAddChatMessage("{2a2cad}[ Calculador | ModTools ]: {FF0000}Jugador nivel 6 o inferior que ha mal usado el /e 1 = Advertencia verbal sea o no por error.", -1)
							elseif 7 <= nvl and nvl <= 9 then
							  sampAddChatMessage("{2a2cad}[ Calculador | ModTools ]: {FF0000}Jugador nivel 9 o inferior que ha mal usado el /e 1 para probar/trolear = Advertencia verbal.", -1)
							  sampAddChatMessage("{2a2cad}[ Calculador | ModTools ]: {FF0000}Jugador nivel 7 o superior que ha mal usado el /e 1 por error = Advertencia.", -1)
							elseif 10 <= nvl and nvl <= 14 then
							  tfinal = tfinal + 30
							  sampAddChatMessage("{2a2cad}[ Calculador | ModTools ]: {FF0000}Jugador nivel 9 o superior que ha mal usado el /e 1 para probar/trolear = 30 minutos.", -1)
							  sampAddChatMessage("{2a2cad}[ Calculador | ModTools ]: {FF0000}Jugador nivel 7 o superior que ha mal usado el /e 1 por error = Advertencia.", -1)
							elseif nvl >= 15 then
							  tfinal = tfinal + 60
							  sampAddChatMessage("{2a2cad}[ Calculador | ModTools ]: {FF0000}Jugador nivel 15 o superior que ha mal usado el /e 1 para probar/trolear = 60 minutos.", -1)
							  sampAddChatMessage("{2a2cad}[ Calculador | ModTools ]: {FF0000}Jugador nivel 7 o superior que ha mal usado el /e 1 por error = Advertencia.", -1)
							end
						  elseif v == "MG2" then
							if 1 <= nvl and nvl <= 5 then
							  sampAddChatMessage("{2a2cad}[ Calculador | ModTools ]: {FF0000}Jugador nivel 5 o inferior que comete MG2 = Advertencia, si sigue = 15 minutos.", -1)
							elseif 6 <= nvl and nvl <= 10 then
							  sampAddChatMessage("{2a2cad}[ Calculador | ModTools ]: {FF0000}Jugador nivel 10 o inferior que comete MG2 = Advertencia, si sigue = 25 minutos.", -1)
							elseif 11 <= nvl and nvl <= 19 then
							  tfinal = tfinal + 30
							  sampAddChatMessage("{2a2cad}[ Calculador | ModTools ]: {FF0000}Jugador nivel 11 o superior que comete MG2 = 30 minutos, si fue error = Advertencia (Muy opcional).", -1)
							elseif nvl >= 20 then
							  tfinal = tfinal + 40
							  sampAddChatMessage("{2a2cad}[ Calculador | ModTools ]: {FF0000}Jugador nivel 20 o superior que comete MG2 = 40 minutos, si fue error = Advertencia (Muy opcional).", -1)
							end
						  elseif v == "IHQ" then
							if nvl >= 1 then
							  sampAddChatMessage("{2a2cad}[ Calculador | ModTools ]: {FF0000}IHQ cualquier nivel = Advertencia verbal, si continua = 20 minutos. Disparar a la HQ = 60 minutos.", -1)
							end
							elseif v == "INCP" then
								if nvl >= 1 then
									sampAddChatMessage("{2a2cad}[ Calculador | ModTools ]: {FF0000}INCP, Rango 1 = Advertencia, Rango 2-6 = 30 min, Rango 7-10 = 50 min, Rango 11+ = 80 min.", -1)
									sampAddChatMessage("{2a2cad}[ Calculador | ModTools ]: {FF0000}Calcula el tiempo total de forma manual ddependiendo del rango.", -1)
								end
							elseif v == "AZS" then
								if nvl >= 1 then
									sampAddChatMessage("{2a2cad}[ Calculador | ModTools ]: {FF0000}AZS, Rango 1 = Advertencia, Rango 2-6 = 30 min, Rango 7-10 = 50 min, Rango 11+ = 80 min.", -1)
									sampAddChatMessage("{2a2cad}[ Calculador | ModTools ]: {FF0000}Calcula el tiempo total de forma manual dependiendo del rango.", -1)
								end
						  elseif v == "ZZ" then
							if 1 <= nvl and nvl <= 6 then
							  tfinal = tfinal + 15
							elseif 7 <= nvl and nvl <= 15 then
							  tfinal = tfinal + 30
							elseif 16 <= nvl and nvl <= 20 then
							  tfinal = tfinal + 45
							elseif nvl >= 21 then
							  tfinal = tfinal + 60
							end
						  elseif v == "NA" then
							if nvl >= 4 and nvl <= 15 then
							  tfinal = tfinal + 50
							elseif 16 <= nvl and nvl <= 20 then
							  tfinal = tfinal + 70
							elseif nvl >= 21 then
							  tfinal = tfinal + 100
							end
							else
							Xinfrac = true
							break
							end
						end
					if tfinal >= 250 then
						tfinal = 250
					end
					if Xinfrac then
						resultado = "{2a2cad}[ Calculador | ModTools ]: {FFD100}Se ha detectado infraccion inexistente. (" .. infracionesc.v .. ")."
					elseif tfinal == 0 and not exinfrac and not binfrac then
						resultado = "{2a2cad}[ Calculador | ModTools ]: {FFD100}Advertencia (" .. infracionesc.v .. "). (Nivel: " .. nivelc.v .. ")."
					elseif binfrac then
						resultado = "{2a2cad}[ Calculador | ModTools ]: {FFFFFF}Tiempo de baneo:{FF0000} " .. tbfinal .. " horas{FFFFFF}  |  Infracciones: {FF0000}" .. infracionesc.v .. "{FFFFFF}  |  Nivel: " .. nivelc.v .. "."
					else
						resultado = "{2a2cad}[ Calculador | ModTools ]: {FFFFFF}Tiempo de sancion:{FF0000} " .. tfinal .. " minutos{FFFFFF}  |  Infracciones: {FF0000}" .. infracionesc.v .. "{FFFFFF}  |  Nivel: " .. nivelc.v .. "."
					end
					sampAddChatMessage(resultado, -1)
					nivelc.v = ""
					infracionesc.v = ""
					end
				end
				imgui.SetCursorPosY(150)
				imgui.TextColoredRGB(u8' Recuerda aplicar los tiempos dependiendo\n de la gravedad de la situaciï¿½n y no\n necesariamente el exacto por nivel!')
				imgui.SetCursorPosY(240)
				imgui.TextColoredRGB(u8'  Si no consigues alguna sancion, mira en la\n  ventana "Sanciones", seguramente este\n  indicada ahi.')
            elseif tab.v == 2 then
				imgui.PushFont(fontyc)
				imgui.TextColoredRGB(u8' Lista de Sanciones/Baneos ')
				imgui.PopFont()
				imgui.TextWrapped(u8[[
				
			  NOMBRE DE SANCIONES (Otros)
			  
MALE1   = Mal uso del "/e 1"

ASIS       = Abuso de Sistemas

TOX       = Toxicidad, palabras poco ofensivas (ez,zzz,feo,bobo,etc)

MALUSO = Mal uso de /yo /accesorios /callsign

AIN		= Abuso de interior

ARAD       = Abuso de Radio de Empresa

El resto de sanciones llevan de nombre los acronimos de siempre, como NRE, DM, SK, PG, AZS, INCP, etc.
				

				BLOQUEOS TEMPORALES

AIOOC: Baneo temporal(grave = permanente).

Macros, Control V + C o usar la flechita:
Primera vez = Advertencia verbal y luego subirlo a #info-users
Segunda vez = Baneo temporal
Tercera vez = Baneo permanente

Cuentas muy troll: Baneo permanente.

NAR: Si no hay mod 4 en adelante, indicar que abra un ticket en /soporte, si no lo hace, aplicar baneo permanente.
Aplicar baneo permanente en caso de ser un nombre Troll.

Estafa: Baneo temporal (Muy grave = permanente).

Suplantacion de identidad: Baneo temporal.


				OTROS

30 Sanciones o mas = Tempban 10 dias.
75 Sanciones + Tban anterior x acumulacion de sanciones > Ban temporal 1 mes o 2 meses, si amerita mas, informaremos.



]])
            elseif tab.v == 3 then
				imgui.PushFont(fontyc)
				imgui.TextColoredRGB(u8' Usuarios Sospechosos  | ')
				imgui.SameLine()
				imgui.TextColoredRGB(u8'? ')
				imgui.PopFont()
				imgui.Hint('hintUS',fa.ICON_FA_SEARCH..' Ayuda: \n Introduce el nombre del usuario junto a una razon en los lugares\n indicados recordando seguir el formato "Nombre_Apellido".\n \nEjemplo:\n Nombre: Juanito_Alimania\n Razon: Cheats - TP \n\n Para eliminar a alguien de la lista solo deberas escribir su nombre\n en minusculas sin especificar alguna razon.\n\n Tambien puedes usar el comando "/ssp" (o presionar el boton "Online")\n para abrir la ventana de sospechosos online!', 0.1)
				imgui.SameLine()
				imgui.SetCursorPosX(217)
				imgui.SetCursorPosY(10)
				if imgui.OutlineButton(u8"Online",  nil) then
					suspe.v = not suspe.v
				end
				imgui.SetCursorPos(imgui.ImVec2(8, 30))
				if imgui.BetterInput("##susname", u8("  Nombre"), susnam, nil, nil, 200) then
					susnam.v = susnam.v:gsub("(.*)", "")
				end
				imgui.SetCursorPos(imgui.ImVec2(8, 70))
				if imgui.BetterInput("##susrazon", u8("  Razon"), susraz, nil, nil, 200) then
					susraz.v = susraz.v:gsub("(.*)", "")
				end
				if imgui.OutlineButton(u8"Agregar!",  nil) then
					addnotify(susnam.v.." "..susraz.v)
					susraz.v = ""
					susnam.v = ""
				end
				imgui.BeginChild('##subsus', imgui.ImVec2(260, 150), true)
				for k, v in pairs(notifytable) do
					local nottext = k.." "..mayusculizador(v.name) .. "|" .. v.reason
					imgui.Text(nottext)
					imgui.SameLine(230)
					if imgui.Button("X "..k.."", imgui.ImVec2(16,23)) then
						removenotify(v.name)
					end
				end
				imgui.EndChild()
            elseif tab.v == 4 then
				imgui.PushFont(fontyc)
				imgui.TextColoredRGB(u8' Informacion de IP  | ')
				imgui.SameLine()
				imgui.TextColoredRGB(u8'? ')
				imgui.PopFont()
				imgui.Hint('hintUS',fa.ICON_FA_SEARCH..' Ayuda: \n Introduce la IP de la que necesites su informacion en el lugar indicado,\n luego pulsa el boton "Ver IP", en caso de ser un VPN, te mostrara un texto\n indicandolo.', 0.1)
				if imgui.BetterInput("##ip", u8("  Ingrese una IP"), ipc, nil, nil, 200) then
					ipc.v = ipc.v
				end
				if imgui.OutlineButton(u8'Ver IP', nil) then
					ip(ipc.v)
				end
				imgui.SameLine()
				if isvpn == "Es VPN!" then
					imgui.PushFont(fontyc)
					imgui.SetCursorPosX(90)
					imgui.SetCursorPosY(65)
					imgui.TextWrapped("\t"..isvpn)
					imgui.PopFont()
					imgui.SetCursorPosX(55)
					imgui.SetCursorPosY(80)
					imgui.PushFont(fontys)
					imgui.TextWrapped("Compruebalo en ipinfo.io pulsando -->")
					imgui.PopFont()
					imgui.SameLine()
					imgui.SetCursorPosY(65)
					if imgui.OutlineButton(u8'IPInfo', nil) then
						if ipc.v then
							os.execute("explorer https://ipinfo.io/" .. ipc.v)
						end
					end
				end
				if isvpn == "Puede ser VPN" then
					imgui.SetCursorPosX(55)
					imgui.SetCursorPosY(69)
					imgui.PushFont(fontys)
					imgui.TextWrapped("Si crees que puede ser falso negativo\ncompruebalo en ipinfo.io pulsando -->")
					imgui.PopFont()
					imgui.SameLine()
					imgui.SetCursorPosY(65)
					if imgui.OutlineButton(u8'IPInfo', nil) then
						if ipc.v then
							os.execute("explorer https://ipinfo.io/" .. ipc.v)
						end
					end
				end
				imgui.SetCursorPosY(100)
				imgui.SetCursorPosX(5)
				imgui.BeginChild('##subip', imgui.ImVec2(260, 193), true)
				imgui.SetCursorPos(imgui.ImVec2(3, -10))
				imgui.TextWrapped(u8:decode""..text2)
				imgui.EndChild()
            elseif tab.v == 5 then
				imgui.PushFont(fontyc)
				imgui.TextColoredRGB(u8' Anti Fail Chat  | ')
				imgui.SameLine()
				imgui.TextColoredRGB(u8'? ')
				imgui.PopFont()
				imgui.Hint('hintUS',fa.ICON_FA_SEARCH..' Ayuda: \n Evita que envies un comando administrativo por error cuando no pones un "\\"\n al principio o si te equivocas de letra, puedes agregar los comandos que desees.\n\n Recuerda seguir el formato, un comando por linea solamente.\n\n Luego de agregar los comandos que desees, presiona en "Guardar" y listo.', 0.1)
				imgui.Text("Lista de comandos:")
				imgui.InputTextMultiline('##imt',commaf, imgui.ImVec2(250,210))
				imgui.SetCursorPosX(45)
				if imgui.OutlineButton(u8'Guardar!', nil) then
					addcmd(commaf.v)
					guardado = true
				end
				imgui.SameLine()
				imgui.SetCursorPosX(120)
				if imgui.OutlineButton('Agregar Auto', nil) then
					aggcmdchat = true
					sampSendChat("/ayuda admin")
				end
				imgui.SameLine()
				if tiemporev and tiemporev > os.time()then
					imgui.PushFont(fontyc)
					imgui.Text("Cargando")
					imgui.PopFont()
				elseif tiemporev ~= nil and tiemporev <= os.time() or guardado then
					imgui.PushFont(fontyc)
					imgui.Text("Hecho!")
					imgui.PopFont()
					lua_thread.create(function() wait(1000) guardado = false end)
				end
            elseif tab.v == 6 then
				imgui.PushFont(fontyc)
				imgui.TextColoredRGB(u8' Registro Anti-Cheat ')
				imgui.PopFont()
				imgui.BeginChild('##subac', imgui.ImVec2(260, 266), true)
				for k, v in pairs(regac) do
					imgui.TextWrapped(os.date("%H:%M:%S",v.fecha).. ' ' ..v.full)
					if imgui.IsItemClicked() then
						addnotify(v.name..' '..v.razon)
					end
				end 
				imgui.EndChild()
            elseif tab.v == 7 then
				imgui.PushFont(fontyc)
				imgui.TextColoredRGB(u8' Configuraciones ')
				imgui.PopFont()
				imgui.Text("")
				if imgui.ToggleButton("#boton2", toggle, "Avisos de Jugadores Sospechosos") then
					ini.settings.susreg = not ini.settings.susreg
					inicfg.save(ini, directIni)
				end
				if imgui.ToggleButton("#boton1", fastspawn, "Fast /Respawncars (Rueda del Mouse)") then
					ini.settings.fastr = not ini.settings.fastr
					inicfg.save(ini, directIni)
				end
				imgui.Hint('hintUS',fa.ICON_FA_SEARCH..' Ayuda: \n Al presionar la "Rueda del Mouse" tu camara se movera hacia atras segun la\n distancia del mouse, a la ves que aparecera un circulo indicando el area\n exacta en la que quieres respawnear los coches cercanos.\n\n Al tener el area que quieres, deberas dar click izquierdo y listo!\n\n En caso de retractarte, presiona de nuevo la Rueda del mouse.', 0.1)
				if imgui.ToggleButton("#boton3", bloqchat, "Bloquear posibles Fail chats") then
					ini.settings.bloqchat = not ini.settings.bloqchat
					inicfg.save(ini, directIni)
				end
				imgui.Hint('hintUS',fa.ICON_FA_SEARCH..' Ayuda: \n Esta funcion bloquearia cualquier comando administrativo enviado de forma\n erronea, como por ejemplo, olvidar el "/" o poner un "7" en su lugar.', 0.1)

				if imgui.ToggleButton("#boton4", wallhack, "WallHack") then
					ini.settings.wh = not ini.settings.wh
					inicfg.save(ini, directIni)
				end
				if ini.settings.wh then
					if imgui.ToggleButton("#boton5", whesque, "Mostrar Esqueleto") then
						ini.settings.whesq = not ini.settings.whesq
						inicfg.save(ini, directIni)
					end
				end
				if imgui.HotKey("##active2", ActiveWall, whLastKeys, 100,20) then
					rkeys.changeHotKey(bindwhID, ActiveWall.v)
					ini.settings.whkey = encodeJson(ActiveWall.v)
					inicfg.save(ini, directIni)
				end
				imgui.SameLine()
				imgui.Text(u8' Activar WallHack')

				if imgui.HotKey("##active", ActiveMenu, tLastKeys, 100,20) then
					rkeys.changeHotKey(bindID, ActiveMenu.v)
					sampAddChatMessage(tag.."Tecla para abrir el menu cambiada. Anterior: {82807f}" .. table.concat(rkeys.getKeysName(tLastKeys.v), " + ") .. "{FFFFFF} | Nueva: {82807f}" .. table.concat(rkeys.getKeysName(ActiveMenu.v), " + "), -1)
					ini.settings.key = encodeJson(ActiveMenu.v)
					inicfg.save(ini, directIni)
				end
				imgui.SameLine()
				imgui.Text(u8' Tecla de acceso rapido')
            elseif tab.v == 8 then
				imgui.PushFont(fontyc)
				imgui.TextColoredRGB(u8' Registro de Cambios ')
				imgui.PopFont()
				imgui.TextWrapped(u8[[
			
Version 2.0
- Eliminada la actualizacion automatica
- Ultima versión, adios

Version 1.95
- Arreglado pequeño error en la config.

Version 1.94
- Agregado WallHack basado en el WallHack de Sobeit
- Agregado tecla de activacion del WallHack configurable

Version 1.93
- Actualizado tiempo de sanciones.
- Agregado "ARAD" para calcular por mal uso de la radio de Empresa

Version 1.9
- Agregado "Anti Fail-Chat", mas informacion en la seccion del mismo nombre
	+ Es posible modificar, agregar o eliminar comandos en su seccion
	+ La lista estará vacia una primera vez, usa "Agregar Auto" para agregar los comandos permitidos segun tu rango y el "/ayuda admin"
	+ Es posible desactivarlo en Configuraciones
- Agregado "Registro AC" el cual guarda un registro de todas las expulsiones recientes por parte del anticheat
	+ Es posible agregar automaticamente a la persona dando click al texto de su expulsion
- Pequeños cambios en la calculadora de sanciones 
- Arreglado codificacion, ahora las letras á, é, í, ó, ú, ñ, etc, apareceran normalmente.

Version 1.8:
- Agregados mejoras al sistema de sospechosos
	+ Ahora puedes borrar alguien de la lista presionando su debido boton
	+ Ahora puedes agregar personas a la lista usando "/sosp {ID} {Razon}"
	+ La lista completa ahora muestra el numero de la persona en la lista y con mayusculas

Version 1.7:
- Cambiada API de comprobacion de VPN a una mas exacta, de igual forma, comprobar las IPs antes de aplicar un baneo a menos que esten 100% seguros de que sea VPN.
- Arreglado "Avisos de Jugadores Sospechosos" no desactivaba los avisos.

Version 1.6:
- Agregados tiempos de PG2 en caso de que el jugador prefiera sancion para no seguir rol.

Version 1.5:
- Arreglado calculo de SK, BA y TK las cuales podian dar un tiempo de sancion inferior con niveles altos.

Version 1.4:
- Agregado "Fast Respawncars".
- Agregada opcion para desactivar "Fast Respawncars" en opciones.

Version 1.3:
- Eliminada funcion de prueba que enviaba la IP al chat luego presionar de usar el boton "IPInfo".
        
Version 1.2:
- Ahora los registros de discord iran a sus respectivos canales en el servidor de discord "Sanciones Administrativas - Galaxy-RP".

Version 1.1: 

- Cambiado sistema de registros en discord por solicitud de la administracion.
- Los registros son enviados a un servidor de discord unico para todos.
- Textos de los "Input" modificados / arreglados para que se vean mejor esteticamente.
- Arreglado tamano de la subventana.
- Agregado aviso de posible falso-negativo al comprobar VPNs (dependiendo del continente).
- Agregado boton para abrir el navegador (Minimizara tu GTA) para comprobar la IP actual en la pagina "ipinfo.io" en caso de un posible falso-negativo.

Version 1.0: 

- Lanzamiento.
				]])
            end
        imgui.EndChild()
    imgui.End()
	end
	if suspe.v then
		imgui.SetNextWindowSize(imgui.ImVec2(220, 120), imgui.Cond.FirstUseEver)
		imgui.SetNextWindowPos(imgui.ImVec2(X / 2 + 300, Y / 2 - 100), imgui.Cond.FirstUseEver, imgui.ImVec2(0.5, 0.5))
		imgui.Begin("Jugadores sospechosos online", suspe, imgui.WindowFlags.NoCollapse)
		maxPlayerOnline = sampGetMaxPlayerId(false)
		for id = 0, maxPlayerOnline, 1 do
			if sampIsPlayerConnected(id) then
				name = sampGetPlayerNickname(id)
				for k, v in pairs(notifytable) do
					if name:lower() == v.name then
					imgui.Text(name .. " (" .. id .. ") - " .. v.reason)
						if imgui.IsItemClicked() then
							sampSendChat("/spec "..id)
						end
					end
				end
			end
		end
    if imgui.Button("Cerrar") then
      suspe.v = false
    end
    imgui.End()
  end
end

drawCircleIn3d = function(x, y, z, radius, color, width, polygons)
    local step = math.floor(360 / (polygons or 36))
    local sX_old, sY_old
	if tonumber(string.format("%02.0f", radius)) >= 50.1 then
	radius = 50
	end
    for angle = 0, 360, step do 
        local _, sX, sY, sZ, _, _ = convert3DCoordsToScreenEx(radius * math.cos(math.rad(angle)) + x , radius * math.sin(math.rad(angle)) + y , z)
        if sZ > 1 then
            if sX_old and sY_old then
                renderDrawLine(sX, sY, sX_old, sY_old, width, color)
            end
            sX_old, sY_old = sX, sY
        end
    end
end
function setCameraDistance(distance)
	memory.setfloat(CCamera + 0xD4, distance)
	memory.setfloat(CCamera + 0xD8, distance)
	memory.setfloat(CCamera + 0xC0, distance)
	memory.setfloat(CCamera + 0xC4, distance)
end

function retCameraDistance(distanceorg)
	if distanceorg >= 5 then
	distanceorg = 5
	end
	memory.setfloat(CCamera + 0xD4, distanceorg)
	memory.setfloat(CCamera + 0xD8, distanceorg)
	memory.setfloat(CCamera + 0xC0, distanceorg)
	memory.setfloat(CCamera + 0xC4, distanceorg)
end 

function imgui.ToggleButton(str_id, bool, label)
    local rBool = false

    if LastActiveTime == nil then
        LastActiveTime = {}
    end
    if LastActive == nil then
        LastActive = {}
    end

    local function ImSaturate(f)
        return f < 0.0 and 0.0 or (f > 1.0 and 1.0 or f)
    end
    
    local p = imgui.GetCursorScreenPos()
    local draw_list = imgui.GetWindowDrawList()

    local height = 20
    local width = 44
    local radius = 5*2
    local rounding = 10.0
    local ANIM_SPEED = 0.15

    if imgui.InvisibleButton(str_id, imgui.ImVec2(width, height)) then
        bool.v = not bool.v
        rBool = true
        LastActiveTime[tostring(str_id)] = os.clock()
        LastActive[tostring(str_id)] = true
    end

    local t = bool.v and 1.0 or 0.0

    if LastActive[tostring(str_id)] then
        local time = os.clock() - LastActiveTime[tostring(str_id)]
        if time <= ANIM_SPEED then
            local t_anim = ImSaturate(time / ANIM_SPEED)
            t = bool.v and t_anim or 1.0 - t_anim
        else
            LastActive[tostring(str_id)] = false
        end
    end

    local col_bg
    local cir_bg
    if bool.v then
        col_bg = imgui.ImColor(204, 204, 204, 255):GetU32()
        cir_bg = imgui.ImColor(100, 100, 100, 255):GetU32()
    else
        col_bg = imgui.ImColor(100, 100, 100, 255):GetU32()
        cir_bg = imgui.ImColor(204, 204, 204, 255):GetU32()
    end
    if bool.v then
        draw_list:AddRectFilled(imgui.ImVec2(p.x, p.y), imgui.ImVec2(p.x + width, p.y + (height)), col_bg, rounding)
    else
        draw_list:AddRectFilled(imgui.ImVec2(p.x, p.y), imgui.ImVec2(p.x + width, p.y + (height)), cir_bg, rounding) --out lines
        draw_list:AddRectFilled(imgui.ImVec2(p.x + 2, p.y + 2), imgui.ImVec2((p.x + width) - 2, (p.y + height) - 2), col_bg, rounding)
    end
    
    draw_list:AddCircleFilled(imgui.ImVec2(p.x + radius + t * (width - radius * 2.0), p.y + radius), radius - ((height - radius)/2), imgui.GetColorU32(bool.v and cir_bg or cir_bg))
    
    if label then
        imgui.SameLine()
        imgui.PushStyleVar(imgui.StyleVar.ItemSpacing, imgui.ImVec2(0.0, 3.0))
        imgui.BeginGroup()
            imgui.Spacing()
            if bool.v then
                imgui.Text(label)
            else
                imgui.Text(label)
            end
            imgui.Spacing()
        imgui.EndGroup()
        imgui.PopStyleVar()
    end
    
    return rBool
end

function imgui.OutlineButton(text,size,set)
    if text == nil or type(text) ~= 'string' then imgui.Text('func imgui.OutlineButton(text,size,set); text = nil-not type string ') return end
    
    size = size or imgui.ImVec2((imgui.CalcTextSize(text).x*2),(imgui.CalcTextSize(text).y*2))
    set = set or {}
    set.Thickness = set.Thickness or 2
    set.Rounding = set.Rounding or 2
    set.Button = set.Button or imgui.GetStyle().Colors[23]
    set.ButtonHovered = set.ButtonHovered or imgui.GetStyle().Colors[24]
    set.Text = set.Text or imgui.GetStyle().Colors[1]

    local p = imgui.GetCursorScreenPos()
    local DL = imgui.GetWindowDrawList()
    local Color = set.Button
    local bool = false

    if imgui.InvisibleButton(text,imgui.ImVec2(size.x/1.9,size.y)) then;    bool = true;    end
    if imgui.IsItemHovered() then;    Color = set.ButtonHovered;    end

    DL:AddText(imgui.ImVec2(p.x+5,p.y+(size.y/2/2)),imgui.ColorConvertFloat4ToU32(set.Text),text)
    DL:AddRect(imgui.ImVec2(p.x,p.y), imgui.ImVec2(p.x+(size.x/2)+10,p.y+size.y), imgui.ColorConvertFloat4ToU32(Color), set.Rounding, nil, set.Thickness)

    return bool
end

function imgui.ButtonHex(lable, rgb, size)
    local r = bit.band(bit.rshift(rgb, 16), 0xFF) / 255
    local g = bit.band(bit.rshift(rgb, 8), 0xFF) / 255
    local b = bit.band(rgb, 0xFF) / 255

    imgui.PushStyleColor(imgui.Col.Button, imgui.ImVec4(r, g, b, 0.6))
    imgui.PushStyleColor(imgui.Col.ButtonHovered, imgui.ImVec4(r, g, b, 0.8))
    imgui.PushStyleColor(imgui.Col.ButtonActive, imgui.ImVec4(r, g, b, 1.0))
    local button = imgui.Button(lable, size)
    imgui.PopStyleColor(3) 
    return button
end

function imgui.CustomMenu(labels, selected, size, speed, centering)
    local bool = false
    speed = speed and speed or 0.2
    local radius = size.y * 0.50
    local draw_list = imgui.GetWindowDrawList()
    if LastActiveTime == nil then LastActiveTime = {} end
    if LastActive == nil then LastActive = {} end
    local function ImSaturate(f)
        return f < 0.0 and 0.0 or (f > 1.0 and 1.0 or f)
    end
    for i, v in ipairs(labels) do
        local c = imgui.GetCursorPos()
        local p = imgui.GetCursorScreenPos()
        if imgui.InvisibleButton(v..'##'..i, size) then
            selected.v = i
            LastActiveTime[v] = os.clock()
            LastActive[v] = true
            bool = true
        end
        imgui.SetCursorPos(c)
        local t = selected.v == i and 1.0 or 0.0
        if LastActive[v] then
            local time = os.clock() - LastActiveTime[v]
            if time <= 0.3 then
                local t_anim = ImSaturate(time / speed)
                t = selected.v == i and t_anim or 1.0 - t_anim
            else
                LastActive[v] = false
            end
        end
        local col_bg = imgui.GetColorU32(selected.v == i and imgui.GetStyle().Colors[imgui.Col.ButtonActive] or imgui.ImVec4(0,0,0,0))
        local col_box = imgui.GetColorU32(selected.v == i and imgui.GetStyle().Colors[imgui.Col.Button] or imgui.ImVec4(0,0,0,0))
        local col_hovered = imgui.GetStyle().Colors[imgui.Col.ButtonHovered]
        local col_hovered = imgui.GetColorU32(imgui.ImVec4(col_hovered.x, col_hovered.y, col_hovered.z, (imgui.IsItemHovered() and 0.2 or 0)))
        draw_list:AddRectFilled(imgui.ImVec2(p.x-size.x/6, p.y), imgui.ImVec2(p.x + (radius * 0.65) + t * size.x, p.y + size.y), col_bg, 10.0)
        draw_list:AddRectFilled(imgui.ImVec2(p.x-size.x/6, p.y), imgui.ImVec2(p.x + (radius * 0.65) + size.x, p.y + size.y), col_hovered, 10.0)
        draw_list:AddRectFilled(imgui.ImVec2(p.x, p.y), imgui.ImVec2(p.x+5, p.y + size.y), col_box)
        imgui.SetCursorPos(imgui.ImVec2(c.x+(centering and (size.x-imgui.CalcTextSize(v).x)/2 or 15), c.y+(size.y-imgui.CalcTextSize(v).y)/2))
        imgui.Text(v)
        imgui.SetCursorPos(imgui.ImVec2(c.x, c.y+size.y))
    end
    return bool
end

function imgui.TextColoredRGB(text)
    local style = imgui.GetStyle()
    local colors = style.Colors
    local ImVec4 = imgui.ImVec4

    local explode_argb = function(argb)
        local a = bit.band(bit.rshift(argb, 24), 0xFF)
        local r = bit.band(bit.rshift(argb, 16), 0xFF)
        local g = bit.band(bit.rshift(argb, 8), 0xFF)
        local b = bit.band(argb, 0xFF)
        return a, r, g, b
    end

    local getcolor = function(color)
        if color:sub(1, 6):upper() == 'SSSSSS' then
            local r, g, b = colors[1].x, colors[1].y, colors[1].z
            local a = tonumber(color:sub(7, 8), 16) or colors[1].w * 255
            return ImVec4(r, g, b, a / 255)
        end
        local color = type(color) == 'string' and tonumber(color, 16) or color
        if type(color) ~= 'number' then return end
        local r, g, b, a = explode_argb(color)
        return imgui.ImColor(r, g, b, a):GetVec4()
    end

    local render_text = function(text_)
        for w in text_:gmatch('[^\r\n]+') do
            local text, colors_, m = {}, {}, 1
            w = w:gsub('{(......)}', '{%1FF}')
            while w:find('{........}') do
                local n, k = w:find('{........}')
                local color = getcolor(w:sub(n + 1, k - 1))
                if color then
                    text[#text], text[#text + 1] = w:sub(m, n - 1), w:sub(k + 1, #w)
                    colors_[#colors_ + 1] = color
                    m = n
                end
                w = w:sub(1, n - 1) .. w:sub(k + 1, #w)
            end
            if text[0] then
                for i = 0, #text do
                    imgui.TextColored(colors_[i] or colors[1], text[i])
                    imgui.SameLine(nil, 0)
                end
                imgui.NewLine()
            else imgui.Text(w) end
        end
    end

    render_text(text)
end

function imgui.BetterInput(name, hint_text, buffer, color, text_color, width)

    
    local function bringVec4To(from, to, start_time, duration)
        local timer = os.clock() - start_time
        if timer >= 0.00 and timer <= duration then
            local count = timer / (duration / 100)
            return imgui.ImVec4(
                from.x + (count * (to.x - from.x) / 100),
                from.y + (count * (to.y - from.y) / 100),
                from.z + (count * (to.z - from.z) / 100),
                from.w + (count * (to.w - from.w) / 100)
            ), true
        end
        return (timer > duration) and to or from, false
    end

    local function bringFloatTo(from, to, start_time, duration)
        local timer = os.clock() - start_time
        if timer >= 0.00 and timer <= duration then
            local count = timer / (duration / 100)
            return from + (count * (to - from) / 100), true
        end
        return (timer > duration) and to or from, false
    end


    
    imgui.SetCursorPosY(imgui.GetCursorPos().y + (imgui.CalcTextSize(hint_text).y * 0.7))


    
    if UI_BETTERINPUT == nil then
        UI_BETTERINPUT = {}
    end
    if not UI_BETTERINPUT[name] then
        UI_BETTERINPUT[name] = {buffer = buffer or imgui.ImBuffer(256), width = nil,
        hint = {
            pos = nil,
            old_pos = nil,
            scale = nil
        },
        color = imgui.GetStyle().Colors[imgui.Col.TextDisabled],
        old_color = imgui.GetStyle().Colors[imgui.Col.TextDisabled],
        active = {false, nil}, inactive = {true, nil}
    }
    end

    local pool = UI_BETTERINPUT[name] 

    
    if color == nil then
        color = imgui.GetStyle().Colors[imgui.Col.ButtonActive]
    end

    if width == nil then
        pool["width"] = imgui.CalcTextSize(hint_text).x + 50
        if pool["width"] < 150 then
            pool["width"] = 150
        end
    else
        pool["width"] = width
    end

    if pool["hint"]["scale"] == nil then
        pool["hint"]["scale"] = 1.0
    end

    if pool["hint"]["pos"] == nil then
        pool["hint"]["pos"] = imgui.ImVec2(imgui.GetCursorPos().x, imgui.GetCursorPos().y)
    end

    if pool["hint"]["old_pos"] == nil then
        pool["hint"]["old_pos"] = imgui.GetCursorPos().y
    end


   
    imgui.PushStyleColor(imgui.Col.FrameBg, imgui.ImVec4(1, 1, 1, 0))
    imgui.PushStyleColor(imgui.Col.Text, text_color or imgui.ImVec4(1, 1, 1, 1))
    imgui.PushStyleColor(imgui.Col.TextSelectedBg, color)
    imgui.PushStyleVar(imgui.StyleVar.FramePadding, imgui.ImVec2(0, imgui.GetStyle().FramePadding.y))
    imgui.PushItemWidth(pool["width"])


    
    local draw_list = imgui.GetWindowDrawList()


    
    draw_list:AddLine(imgui.ImVec2(imgui.GetCursorPos().x + imgui.GetWindowPos().x,
    imgui.GetCursorPos().y + imgui.GetWindowPos().y + (2 * imgui.GetStyle().FramePadding.y) + imgui.CalcTextSize(hint_text).y),
    imgui.ImVec2(imgui.GetCursorPos().x + imgui.GetWindowPos().x + pool["width"],
    imgui.GetCursorPos().y + imgui.GetWindowPos().y + (2 * imgui.GetStyle().FramePadding.y) + imgui.CalcTextSize(hint_text).y),
    imgui.GetColorU32(pool["color"]), 2.0)


    
    imgui.InputText("##" .. name, pool["buffer"])


    
    if not imgui.IsItemActive() then
        if pool["inactive"][2] == nil then pool["inactive"][2] = os.clock() end
        pool["inactive"][1] = true
        pool["active"][1] = false
        pool["active"][2] = nil

    elseif imgui.IsItemActive() or imgui.IsItemClicked() then
        pool["inactive"][1] = false
        pool["inactive"][2] = nil
        if pool["active"][2] == nil then pool["active"][2] = os.clock() end
        pool["active"][1] = true
    end
    
    if pool["inactive"][1] and #pool["buffer"].v == 0 then
        pool["color"] = bringVec4To(pool["color"], pool["old_color"], pool["inactive"][2], 0.75)
        pool["hint"]["scale"] = bringFloatTo(pool["hint"]["scale"], 1.0, pool["inactive"][2], 0.25)
        pool["hint"]["pos"].y = bringFloatTo(pool["hint"]["pos"].y, pool["hint"]["old_pos"], pool["inactive"][2], 0.25)
        
    elseif pool["inactive"][1] and #pool["buffer"].v > 0 then
        pool["color"] = bringVec4To(pool["color"], pool["old_color"], pool["inactive"][2], 0.75)
        pool["hint"]["scale"] = bringFloatTo(pool["hint"]["scale"], 0.7, pool["inactive"][2], 0.25)
        pool["hint"]["pos"].y = bringFloatTo(pool["hint"]["pos"].y, pool["hint"]["old_pos"] - (imgui.GetFontSize() * 0.7) - 2,
        pool["inactive"][2], 0.25)

    elseif pool["active"][1] and #pool["buffer"].v == 0 then
        pool["color"] = bringVec4To(pool["color"], color, pool["active"][2], 0.75)
        pool["hint"]["scale"] = bringFloatTo(pool["hint"]["scale"], 0.7, pool["active"][2], 0.25)
        pool["hint"]["pos"].y = bringFloatTo(pool["hint"]["pos"].y, pool["hint"]["old_pos"] - (imgui.GetFontSize() * 0.7) - 2,
        pool["active"][2], 0.25)

    elseif pool["active"][1] and #pool["buffer"].v > 0 then
        pool["color"] = bringVec4To(pool["color"], color, pool["active"][2], 0.75)
        pool["hint"]["scale"] = bringFloatTo(pool["hint"]["scale"], 0.7, pool["active"][2], 0.25)
        pool["hint"]["pos"].y = bringFloatTo(pool["hint"]["pos"].y, pool["hint"]["old_pos"] - (imgui.GetFontSize() * 0.7) - 2,
        pool["active"][2], 0.25)
    end   
    imgui.SetWindowFontScale(pool["hint"]["scale"])
    
    
    
    draw_list:AddText(imgui.ImVec2(pool["hint"]["pos"].x + imgui.GetWindowPos().x + imgui.GetStyle().FramePadding.x,
    pool["hint"]["pos"].y + imgui.GetWindowPos().y + imgui.GetStyle().FramePadding.y),
    imgui.GetColorU32(pool["color"]),
    hint_text)


    
    imgui.SetWindowFontScale(1.0)
    imgui.PopItemWidth()
    imgui.PopStyleColor(3)
    imgui.PopStyleVar()
end

function imgui.Hint(str_id, hint, delay)
    local hovered = imgui.IsItemHovered()
    local animTime = 0.2
    local delay = delay or 0.00
    local show = true

    if not allHints then allHints = {} end
    if not allHints[str_id] then
        allHints[str_id] = {
            status = false,
            timer = 0
        }
    end

    if hovered then
        for k, v in pairs(allHints) do
            if k ~= str_id and os.clock() - v.timer <= animTime  then
                show = false
            end
        end
    end

    if show and allHints[str_id].status ~= hovered then
        allHints[str_id].status = hovered
        allHints[str_id].timer = os.clock() + delay
    end

    if show then
        local between = os.clock() - allHints[str_id].timer
        if between <= animTime then
            local s = function(f)
                return f < 0.0 and 0.0 or (f > 1.0 and 1.0 or f)
            end
            local alpha = hovered and s(between / animTime) or s(1.00 - between / animTime)
            imgui.PushStyleVar(imgui.StyleVar.Alpha, alpha)
            imgui.SetTooltip(hint)
            imgui.PopStyleVar()
        elseif hovered then
            imgui.SetTooltip(hint)
        end
    end
end

function imgui.RoundButton(str_id, radius, round_float)
    radius = radius or 10
    round_float = round_float or 8.0
    imgui.PushStyleVar(imgui.StyleVar.FrameRounding, round_float)
    local roundbtnres = imgui.Button(str_id,imgui.ImVec2(radius*2,radius*2))
    imgui.PopStyleVar(1)
    return roundbtnres
end

function style2()
    imgui.SwitchContext()

    local style = imgui.GetStyle()
    local colors = style.Colors
    local clr = imgui.Col
    local ImVec4 = imgui.ImVec4
    local ImVec2 = imgui.ImVec2
	
    --==[ STYLE ]==--
    imgui.GetStyle().WindowPadding = imgui.ImVec2(5, 5)
    imgui.GetStyle().FramePadding = imgui.ImVec2(5, 5)
    imgui.GetStyle().ItemSpacing = imgui.ImVec2(6, 6)
    imgui.GetStyle().ItemInnerSpacing = imgui.ImVec2(6, 6)
    imgui.GetStyle().TouchExtraPadding = imgui.ImVec2(5, 5)
    imgui.GetStyle().IndentSpacing = 5
    imgui.GetStyle().ScrollbarSize = 10
    imgui.GetStyle().GrabMinSize = 17

    --==[ ROUNDING ]==--
    imgui.GetStyle().WindowRounding = 6
    imgui.GetStyle().FrameRounding = 4
    imgui.GetStyle().ScrollbarRounding = 4
    imgui.GetStyle().GrabRounding = 2
	
    --==[ ALIGN ]==--
    imgui.GetStyle().WindowTitleAlign = imgui.ImVec2(0.5, 0.5)
    imgui.GetStyle().ButtonTextAlign = imgui.ImVec2(0.5, 0.5)
    
colors[clr.Text] = ImVec4(0.80, 0.80, 0.83, 1.00);
    colors[clr.TextDisabled] = ImVec4(0.24, 0.23, 0.29, 1.00);
    colors[clr.WindowBg] = ImVec4(0.06, 0.05, 0.07, 1.00);
    colors[clr.ChildWindowBg] = ImVec4(0.07, 0.07, 0.09, 1.00);
    colors[clr.PopupBg] = ImVec4(0.07, 0.07, 0.09, 1.00);
    colors[clr.Border] = ImVec4(0.80, 0.80, 0.83, 0.88);
    colors[clr.BorderShadow] = ImVec4(0.92, 0.91, 0.88, 0.00);
    colors[clr.FrameBg] = ImVec4(0.10, 0.09, 0.12, 1.00);
    colors[clr.FrameBgHovered] = ImVec4(0.24, 0.23, 0.29, 1.00);
    colors[clr.FrameBgActive] = ImVec4(0.56, 0.56, 0.58, 1.00);
    colors[clr.TitleBg] = ImVec4(0.10, 0.09, 0.12, 1.00);
    colors[clr.TitleBgCollapsed] = ImVec4(1.00, 0.98, 0.95, 0.75);
    colors[clr.TitleBgActive] = ImVec4(0.07, 0.07, 0.09, 1.00);
    colors[clr.MenuBarBg] = ImVec4(0.10, 0.09, 0.12, 1.00);
    colors[clr.ScrollbarBg] = ImVec4(0.10, 0.09, 0.12, 1.00);
    colors[clr.ScrollbarGrab] = ImVec4(0.80, 0.80, 0.83, 0.31);
    colors[clr.ScrollbarGrabHovered] = ImVec4(0.56, 0.56, 0.58, 1.00);
    colors[clr.ScrollbarGrabActive] = ImVec4(0.06, 0.05, 0.07, 1.00);
    --colors[clr.ComboBg] = ImVec4(0.19, 0.18, 0.21, 1.00);
    colors[clr.CheckMark] = ImVec4(0.80, 0.80, 0.83, 0.31);
    colors[clr.SliderGrab] = ImVec4(0.80, 0.80, 0.83, 0.31);
    colors[clr.SliderGrabActive] = ImVec4(0.06, 0.05, 0.07, 1.00);
    colors[clr.Button] = ImVec4(0.10, 0.09, 0.12, 1.00);
    colors[clr.ButtonHovered] = ImVec4(0.24, 0.23, 0.29, 1.00);
    colors[clr.ButtonActive] = ImVec4(0.56, 0.56, 0.58, 1.00);
    colors[clr.Header] = ImVec4(0.10, 0.09, 0.12, 1.00);
    colors[clr.HeaderHovered] = ImVec4(0.56, 0.56, 0.58, 1.00);
    colors[clr.HeaderActive] = ImVec4(0.06, 0.05, 0.07, 1.00);
    --colors[clr.Column] = ImVec4(0.56, 0.56, 0.58, 1.00);
    --colors[clr.ColumnHovered] = ImVec4(0.24, 0.23, 0.29, 1.00);
    --colors[clr.ColumnActive] = ImVec4(0.56, 0.56, 0.58, 1.00);
    colors[clr.ResizeGrip] = ImVec4(0.00, 0.00, 0.00, 0.00);
    colors[clr.ResizeGripHovered] = ImVec4(0.56, 0.56, 0.58, 1.00);
    colors[clr.ResizeGripActive] = ImVec4(0.06, 0.05, 0.07, 1.00);
    --colors[clr.CloseButton] = ImVec4(0.40, 0.39, 0.38, 0.16);
    --colors[clr.CloseButtonHovered] = ImVec4(0.40, 0.39, 0.38, 0.39);
    --colors[clr.CloseButtonActive] = ImVec4(0.40, 0.39, 0.38, 1.00);
    colors[clr.PlotLines] = ImVec4(0.40, 0.39, 0.38, 0.63);
    colors[clr.PlotLinesHovered] = ImVec4(0.25, 1.00, 0.00, 1.00);
    colors[clr.PlotHistogram] = ImVec4(0.40, 0.39, 0.38, 0.63);
    colors[clr.PlotHistogramHovered] = ImVec4(0.25, 1.00, 0.00, 1.00);
    colors[clr.TextSelectedBg] = ImVec4(0.25, 1.00, 0.00, 0.43);
    colors[clr.ModalWindowDarkening] = ImVec4(1.00, 0.98, 0.95, 0.73);
end

function asyncHttpRequest(method, url, args, resolve, reject)
	local request_thread = effil.thread(function(method, url, args)
		local requests = require"requests"
		local result, response = pcall(requests.request, method, url, args)
		if result then
			response.json, response.xml = nil, nil
			return true, response
		else
			return false, response
		end
	end)(method, url, args)

	if not resolve then
		resolve = function() end
	end
	if not reject then
		reject = function() end
	end
	lua_thread.create(function()
		local runner = request_thread
		while true do
			local status, err = runner:status()
			if not err then
				if status == "completed" then
					local result, response = runner:get()
					if result then
						resolve(response)
					else
						reject(response)
					end
					return
				elseif status == "canceled" then
					return reject(status)
				end
			else
				return reject(err)
			end
			wait(0)
		end
	end)
end

function getBodyPartCoordinates(id, handle)
  local pedptr = getCharPointer(handle)
  local vec = ffi.new("float[3]")
  getBonePosition(ffi.cast("void*", pedptr), vec, id, true)
  return vec[0], vec[1], vec[2]
end

function ip(cl)
	ips = {}
	local ipsita = string.match(tostring(cl), "(%d+%p%d+%p%d+%p%d+)")
	if ipsita == nil then
		text2 = "\n\tDireccion IP invalida."
	end
	for word in string.gmatch(tostring(cl), "(%d+%p%d+%p%d+%p%d+)") do
		table.insert(ips, { query = word })
	end
	if #ips > 0 then
		ip1 = word
		data_json = cjson.encode(ips)
		asyncHttpRequest(
			"POST",
			"https://ipqualityscore.com/api/json/ip/hRpc6B9WXCKAJJL7ruFqBc6lemDWs2cL/"..ipsita.."?strictness=1&allow_public_access_points=true",
			nil,
			function(response)
				local rdata = cjson.decode(response.text)
				rdata = table_to_string(rdata)
				local text = ""
				local finded = string.match(rdata, 'message"]="(%a+)"')
				if finded == "Success" then
					text =  
						text .. string.format(u8:decode"\nIP - %s\nEstado - %s\nHost - %s\nCodigo de Pais - %s\nRegion - %s\nCiudad - %s\nZona horaria - %s\nProveedor - %s\nOrganizacion - %s\nASN - %s\nConexion desde celular - %s\nProxy - %s\nVPN - %s\nIncidentes Recientes - %s\n\n",
							ipsita,
							string.match(rdata, 'message"]="(%a+)"'),
							string.match(rdata, '"host"]="(.-)"'),
							string.match(rdata, '"country_code"]="(.-)"'),
							string.match(rdata, '"region"]="(.-)"'),
							string.match(rdata, '"city"]="(.-)"'),
							string.match(rdata, '"timezone"]="(.-)"'),
							string.match(rdata, '"ISP"]="(.-)"'),
							string.match(rdata, '"organization"]="(.-)"'),
							string.match(rdata, '"ASN"]="(.-)"'),
							string.match(rdata, '"mobile"]=(.-),'),
							string.match(rdata, '"proxy"]=(.-),'),
							string.match(rdata, '"vpn"]=(.-),'),
							string.match(rdata, '"recent_abuse"]=(.-),')
						)
						vpn = string.match(rdata, '"vpn"]=(.-),')
						if vpn == "true"  then
							isvpn = "Es VPN!"
						else
							isvpn = "Puede ser VPN"
						end	
				else 
					text = u8:decode" \n\tNo se encontro nada"
				end
				text2 = (u8:decode""..text)
				ipverfi = true
			end,
			function(err)
				text2 = "\n\tOcurrio un error, intenta de nuevo."
			end
		)
	end
end

function mayusculizador(nombre)
	local prila, restA, prilb, restB = nombre:match("^(.)(.-)_(.)(.-)$")
	local lminus = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"}
	local lmayus = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"}
	local result = ""
	for i = 1, 26 do
		if prila == lminus[i] then
			prila = lmayus[i]
		end
	end
	for i = 1, 26 do
		if prilb == lminus[i] then
			prilb = lmayus[i]
		end
	end
	result = prila..restA.."_"..prilb..restB
	return result
end

function table_to_string(tbl)
    local result = "{"
    for k, v in pairs(tbl) do
        if type(k) == "string" then
            result = result.."[\""..k.."\"]".."="
        end
        if type(v) == "table" then
            result = result..table_to_string(v)
        elseif type(v) == "boolean" then
            result = result..tostring(v)
        else
            result = result.."\""..v.."\""
        end
        result = result..","
    end
    if result ~= "" then
        result = result:sub(1, result:len()-1)
    end
    return result.."}"
end
function imgui.Link(link, text)
    text = text or link
    local tSize = imgui.CalcTextSize(text)
    local p = imgui.GetCursorScreenPos()
    local DL = imgui.GetWindowDrawList()
    local col = { 0xFFFF7700, 0xFFFF9900 }
    if imgui.InvisibleButton("##" .. link, tSize) then os.execute("explorer " .. link) end
    local color = imgui.IsItemHovered() and col[1] or col[2]
    DL:AddText(p, color, text)
    DL:AddLine(imgui.ImVec2(p.x, p.y + tSize.y), imgui.ImVec2(p.x + tSize.x, p.y + tSize.y), color)
end
reasons = {
  [0] = "Crash/Caida de conexion",
  "/Quit | Desconexion voluntaria",
  "Expulsado/Baneado del servidor"
}
function sampev.onPlayerJoin(id, color, npc, nombre)
  if ini.settings.susreg == false then
    return 
  end
  for k, v in pairs(notifytable) do
    if nombre:lower() == v.name:lower() then
      sampAddChatMessage(tag.."Usuario sospechoso: {FFFFFF}{34eb46}" .. nombre .. " (" .. id .. ")" .. "{FFFFFF} acaba de conectar!", 16777215)
      return 
    end
  end
end
function sampev.onPlayerQuit(id, reason)
	if ini.settings.susreg == false then
		return 
	end
	local nombre = sampGetPlayerNickname(id)
	for k, v in pairs(notifytable) do
    if nombre:lower() == v.name:lower() then
		sampAddChatMessage(tag.."Usuario sospechoso: {e61920}" .. nombre .. " {FFFFFF}ha salido del servidor. Razon: " .. reasons[reason] .. ".", 16777215)
		return 
	end
	end
end
function addnotify(arg)
  -- line: [0, 0] id: 48
	local nombre, razon = arg:match("^(%S+)%s*(.-)$")
	if not nombre or not razon then
		sampAddChatMessage(tag.."Debes proporcionar un nombre y una razon.", 15079712)
		return 
	end
	if not nombre:find("_") then
		sampAddChatMessage(tag.."El nombre debe seguir el formato Nombre_Apellido.", 15079712)
		return 
	end
	for k, v in pairs(notifytable) do
		if v.name:lower() == nombre:lower() then
			return
		end
	end
	if razon == '' then
		for k, v in pairs(notifytable) do
			if v.name:lower() == nombre:lower() then
				removenotify(nombre)
			end
		end
	return end
	notifydb = io.open(fileS, "a+")
	io.output(notifydb)
	io.write(nombre .. " " .. razon .. "\n")
	io.close(notifydb)
	table.insert(notifytable, {
		name = nombre:lower(),
		reason = razon,
	})
	sampAddChatMessage(tag.."Has agregado a {e61920}" .. nombre .. "{FFFFFF} a la lista de sospechosos. Razon: " .. razon .. ".", 16777215)
end
function removenotify(arg)
  i = 1
  for k, v in pairs(notifytable) do
    if v.name:lower() == arg then
      table.remove(notifytable, i)
      sampAddChatMessage(tag.."{e61920}" .. mayusculizador(arg) .. "{FFFFFF} ha sido eliminado de la lista de sospechosos.", 16777215)
    end
    i = i + 1
  end
  notifydb = io.open(fileS, "w")
  io.output(notifydb)
  for k, v in pairs(notifytable) do
    io.write(v.name .. " " .. v.reason .. "\n")
  end
  io.close(notifydb)
end

function addcmd(arg)
	cmdfile = io.open(fileL, "w")
	io.output(cmdfile)
	io.write(arg)
	io.close(cmdfile)
	cmdtable = {}
	cmdfile = io.open(fileL, "a+")
	for todo in cmdfile.lines(cmdfile) do
		local com = todo:match("^(%a+)")
		if com ~= "a" then
		table.insert(cmdtable, com)
		end
	end
	io.close(cmdfile)
end

function sampev.onSendChat(msg)
	local texto = msg
	lua_thread.create(function() 
		while texto:sub(1,1) == " " do
			texto = texto:sub(2,#texto)
		end
	end)
	local _,comm,ex = texto:match("^(.-)(%a-)%s(.+)")
	if not comm or comm == " " then
		_,comm,ex = texto:match("^(.-)(%a-)%s(.+)")
	end
	if ex then
	comm = comm.. ' ' ..ex
	elseif not ex and comm then
	comm = comm.. ' '
	end
	if ini.settings.bloqchat and comm ~= " " and comm ~= nil then
		comm = comm:lower()
		if comm:sub(1,1) ~= "/" or comm:sub(1,2) == " /" then
			if comm:sub(1,1) == "7" or comm:sub(1,1) == " " then
			comm = comm:sub(2,#comm+1)
			elseif comm:sub(1,2) == " /" then
			comm = comm:sub(3,#comm+1)
			end
			for i = 1, #cmdtable  do
				if comm:find('^'..cmdtable[i]:lower()..' ') then
					sampAddChatMessage(tag..'Fail chat bloqueado. Texto: "'.. msg ..'".')
					return false
				end
			end
		end
	end
end

